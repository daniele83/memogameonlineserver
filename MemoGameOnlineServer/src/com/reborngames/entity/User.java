package com.reborngames.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String username;
	private String password;
	private String facebookId;
	private String facebookPictureURL;
	private String email;
	private Integer image;

	private Integer level = 1;
	private Integer exp = 0;
	private Integer pills = 0;

	private Integer powerUp_Time = 0;
	private Integer powerUp_DoubleCouple = 0;
	private Integer powerUp_FaceUp = 0;
	private Integer powerUp_Stop = 0;
	private Integer powerUp_Chain = 0;
	private Integer powerUp_Bonus = 0;

	private Integer openGames = 0;
	private Integer playedGames = 0;
	private Integer givenUpMatches = 0;
	private Integer wonGames = 0;
	private Integer consecutiveMatchWon = 0;
	private Integer tempConsecutiveMatchesWon = 0;
	private Integer highestmatchScore = 0;
	private Integer highestFirstRoundScore;
	private float bestFirstRoundTime = 0;
	private Integer highestSecondRoundScore = 0;
	private float bestSecondRoundTime = 0;
	private Integer highestThirdRoundScore = 0;
	private float bestThirdRoundTime = 0;
	
	private Long firstRoundSum=0l;
	private Long secondRoundSum=0l;
	private Long thirdRoundSum=0l;
	
	private Integer firstRoundCounter=0;
	private Integer secondRoundCounter=0;
	private Integer thirdRoundCounter=0;

	private Boolean bot;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getImage() {
		return image;
	}

	public void setImage(Integer image) {
		this.image = image;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public boolean isBot() {
		return bot;
	}

	public Integer getConsecutiveMatchWon() {
		return consecutiveMatchWon;
	}

	public void setConsecutiveMatchWon(Integer consecutiveMatchWon) {
		this.consecutiveMatchWon = consecutiveMatchWon;
	}

	public Integer getHighestmatchScore() {
		return highestmatchScore;
	}

	public void setHighestmatchScore(Integer highestmatchScore) {
		this.highestmatchScore = highestmatchScore;
	}

	public Integer getHighestFirstRoundScore() {
		return highestFirstRoundScore;
	}

	public void setHighestFirstRoundScore(Integer highestFirstRoundScore) {
		this.highestFirstRoundScore = highestFirstRoundScore;
	}

	public float getBestFirstRoundTime() {
		return bestFirstRoundTime;
	}

	public void setBestFirstRoundTime(float bestFirstRoundTime) {
		this.bestFirstRoundTime = bestFirstRoundTime;
	}

	public Integer getHighestSecondRoundScore() {
		return highestSecondRoundScore;
	}

	public void setHighestSecondRoundScore(Integer highestSecondRoundScore) {
		this.highestSecondRoundScore = highestSecondRoundScore;
	}

	public float getBestSecondRoundTime() {
		return bestSecondRoundTime;
	}

	public void setBestSecondRoundTime(float bestSecondRoundTime) {
		this.bestSecondRoundTime = bestSecondRoundTime;
	}

	public Integer getHighestThirdRoundScore() {
		return highestThirdRoundScore;
	}

	public void setHighestThirdRoundScore(Integer highestThirdRoundScore) {
		this.highestThirdRoundScore = highestThirdRoundScore;
	}

	public float getBestThirdRoundTime() {
		return bestThirdRoundTime;
	}

	public void setBestThirdRoundTime(float bestThirdRoundTime) {
		this.bestThirdRoundTime = bestThirdRoundTime;
	}

	public Integer getPlayedGames() {
		return playedGames;
	}

	public void setPlayedGames(Integer playedGames) {
		this.playedGames = playedGames;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public Integer getWonGames() {
		return wonGames;
	}

	public void setWonGames(Integer wonGames) {
		this.wonGames = wonGames;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getExp() {
		return exp;
	}

	public void setExp(Integer exp) {
		this.exp = exp;
	}

	public Integer getPills() {
		return pills;
	}

	public Integer getOpenGames() {
		return openGames;
	}

	public void setOpenGames(Integer openGames) {
		this.openGames = openGames;
	}

	public Integer getGivenUpMatches() {
		return givenUpMatches;
	}

	public void setGivenUpMatches(Integer givenUpMatches) {
		this.givenUpMatches = givenUpMatches;
	}

	public Integer getPowerUp_Time() {
		return powerUp_Time;
	}

	public void setPowerUp_Time(Integer powerUp_Time) {
		this.powerUp_Time = powerUp_Time;
	}

	public Integer getPowerUp_DoubleCouple() {
		return powerUp_DoubleCouple;
	}

	public void setPowerUp_DoubleCouple(Integer powerUp_DoubleCouple) {
		this.powerUp_DoubleCouple = powerUp_DoubleCouple;
	}

	public Integer getPowerUp_FaceUp() {
		return powerUp_FaceUp;
	}

	public void setPowerUp_FaceUp(Integer powerUp_FaceUp) {
		this.powerUp_FaceUp = powerUp_FaceUp;
	}

	public Integer getPowerUp_Stop() {
		return powerUp_Stop;
	}

	public void setPowerUp_Stop(Integer powerUp_Stop) {
		this.powerUp_Stop = powerUp_Stop;
	}

	public Integer getPowerUp_Chain() {
		return powerUp_Chain;
	}

	public void setPowerUp_Chain(Integer powerUp_Chain) {
		this.powerUp_Chain = powerUp_Chain;
	}

	public Integer getPowerUp_Bonus() {
		return powerUp_Bonus;
	}

	public void setPowerUp_Bonus(Integer powerUp_Bonus) {
		this.powerUp_Bonus = powerUp_Bonus;
	}

	public Boolean getBot() {
		return bot;
	}

	public void setBot(Boolean bot) {
		this.bot = bot;
	}

	public void setPills(Integer pills) {
		this.pills = pills;
	}

	public Integer getTempConsecutiveMatchesWon() {
		return tempConsecutiveMatchesWon;
	}

	public void setTempConsecutiveMatchesWon(Integer tempConsecutiveMatchesWon) {
		this.tempConsecutiveMatchesWon = tempConsecutiveMatchesWon;
	}

	public String getFacebookPictureURL() {
		return facebookPictureURL;
	}

	public void setFacebookPictureURL(String facebookPictureURL) {
		this.facebookPictureURL = facebookPictureURL;
	}

	public Long getFirstRoundSum() {
		return firstRoundSum;
	}

	public void setFirstRoundSum(Long firstRoundSum) {
		this.firstRoundSum = firstRoundSum;
	}

	public Long getSecondRoundSum() {
		return secondRoundSum;
	}

	public void setSecondRoundSum(Long secondRoundSum) {
		this.secondRoundSum = secondRoundSum;
	}

	public Long getThirdRoundSum() {
		return thirdRoundSum;
	}

	public void setThirdRoundSum(Long thirdRoundSum) {
		this.thirdRoundSum = thirdRoundSum;
	}

	public Integer getFirstRoundCounter() {
		return firstRoundCounter;
	}

	public void setFirstRoundCounter(Integer firstRoundCounter) {
		this.firstRoundCounter = firstRoundCounter;
	}

	public Integer getSecondRoundCounter() {
		return secondRoundCounter;
	}

	public void setSecondRoundCounter(Integer secondRoundCounter) {
		this.secondRoundCounter = secondRoundCounter;
	}

	public Integer getThirdRoundCounter() {
		return thirdRoundCounter;
	}

	public void setThirdRoundCounter(Integer thirdRoundCounter) {
		this.thirdRoundCounter = thirdRoundCounter;
	}
	
	

}
