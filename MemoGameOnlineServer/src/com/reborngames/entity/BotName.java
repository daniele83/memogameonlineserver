package com.reborngames.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "botname")
public class BotName {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String username="";
	private Boolean alreadyUsed=false;

	public BotName() {
	}

	public BotName(String name) {

		username = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getAlreadyUsed() {
		return alreadyUsed;
	}

	public void setAlreadyUsed(Boolean alreadyUsed) {
		this.alreadyUsed = alreadyUsed;
	}

}
