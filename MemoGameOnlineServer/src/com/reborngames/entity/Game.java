package com.reborngames.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.Gson;

@Entity
@Table(name = "game")
public class Game {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String gameState; // WaitingRoundOne , WaitingRoundTwo ,
								// WaitingRoundThree , GameEnded
	private String playerOne = "";
	private String playerTwo = "";

	private Integer playerOneScoreOne = -1;
	private Integer playerOneScoreTwo = -1;
	private Integer playerOneScoreThree = -1;

	private Integer playerTwoScoreOne = -1;
	private Integer playerTwoScoreTwo = -1;
	private Integer playerTwoScoreThree = -1;

	private String firstPlayerRejected = "";
	private String secondPlayerRejected = "";
	private String thirdPlayerRejected = "";
	private String fourthPlayerRejected = "";
	private String fifthPlayerRejected = "";

	private Integer timesHasBeenRefused = 0;

	private boolean closed;
	private boolean closedForGiveUp;
	private String playerWhoGaveUp = "";
	private boolean playerOneReceivedPills;
	private boolean playerTwoReceivedPills;
	private boolean playerOneRequestedDelete;
	private boolean playerTwoRequestedDelete;

	private String chatString = "";

	public Game(String playerOne) {
		this.playerOne = playerOne;
		this.gameState = "WaitingRoundOne";
	}

	public Game(long game_id, String playerOne, String playerTwo) {
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.gameState = "WaitingRoundOne";
	}

	public Game() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGameState() {
		return gameState;
	}

	public void setGameState(String gameState) {
		this.gameState = gameState;
	}

	public String getPlayerOne() {
		return playerOne;
	}

	public void setPlayerOne(String playerOne) {
		this.playerOne = playerOne;
	}

	public String getPlayerTwo() {
		return playerTwo;
	}

	public void setPlayerTwo(String playerTwo) {
		this.playerTwo = playerTwo;
	}

	public Integer getPlayerOneScoreOne() {
		return playerOneScoreOne;
	}

	public boolean isClosedForGiveUp() {
		return closedForGiveUp;
	}

	public void setClosedForGiveUp(boolean closedForGiveUp) {
		this.closedForGiveUp = closedForGiveUp;
	}

	public void setPlayerOneScoreOne(Integer playerOneScoreOne) {
		this.playerOneScoreOne = playerOneScoreOne;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public Integer getPlayerOneScoreTwo() {
		return playerOneScoreTwo;
	}

	public void setPlayerOneScoreTwo(Integer playerOneScoreTwo) {
		this.playerOneScoreTwo = playerOneScoreTwo;
	}

	public Integer getPlayerOneScoreThree() {
		return playerOneScoreThree;
	}

	public void setPlayerOneScoreThree(Integer playerOneScoreThree) {
		this.playerOneScoreThree = playerOneScoreThree;
	}

	public Integer getPlayerTwoScoreOne() {
		return playerTwoScoreOne;
	}

	public void setPlayerTwoScoreOne(Integer playerTwoScoreOne) {
		this.playerTwoScoreOne = playerTwoScoreOne;
	}

	public Integer getPlayerTwoScoreTwo() {
		return playerTwoScoreTwo;
	}

	public void setPlayerTwoScoreTwo(Integer playerTwoScoreTwo) {
		this.playerTwoScoreTwo = playerTwoScoreTwo;
	}

	public Integer getPlayerTwoScoreThree() {
		return playerTwoScoreThree;
	}

	public void setPlayerTwoScoreThree(Integer playerTwoScoreThree) {
		this.playerTwoScoreThree = playerTwoScoreThree;
	}

	public Integer getTimesHasBeenRefused() {
		return timesHasBeenRefused;
	}

	public void setTimesHasBeenRefused(Integer timesHasBeenRefused) {
		this.timesHasBeenRefused = timesHasBeenRefused;
	}

	public String getPlayerWhoGaveUp() {
		return playerWhoGaveUp;
	}

	public void setPlayerWhoGaveUp(String playerWhoGaveUp) {
		this.playerWhoGaveUp = playerWhoGaveUp;
	}

	public String getFirstPlayerRejected() {
		return firstPlayerRejected;
	}

	public void setFirstPlayerRejected(String firstPlayerRejected) {
		this.firstPlayerRejected = firstPlayerRejected;
	}

	public String getSecondPlayerRejected() {
		return secondPlayerRejected;
	}

	public void setSecondPlayerRejected(String secondPlayerRejected) {
		this.secondPlayerRejected = secondPlayerRejected;
	}

	public String getThirdPlayerRejected() {
		return thirdPlayerRejected;
	}

	public void setThirdPlayerRejected(String thirdPlayerRejected) {
		this.thirdPlayerRejected = thirdPlayerRejected;
	}

	public String getFourthPlayerRejected() {
		return fourthPlayerRejected;
	}

	public void setFourthPlayerRejected(String fourthPlayerRejected) {
		this.fourthPlayerRejected = fourthPlayerRejected;
	}

	public String getFifthPlayerRejected() {
		return fifthPlayerRejected;
	}

	public void setFifthPlayerRejected(String fifthPlayerRejected) {
		this.fifthPlayerRejected = fifthPlayerRejected;
	}

	public boolean isPlayerOneRequestedDelete() {
		return playerOneRequestedDelete;
	}

	public void setPlayerOneRequestedDelete(boolean playerOneRequestedDelete) {
		this.playerOneRequestedDelete = playerOneRequestedDelete;
	}

	public boolean isPlayerTwoRequestedDelete() {
		return playerTwoRequestedDelete;
	}

	public void setPlayerTwoRequestedDelete(boolean playerTwoRequestedDelete) {
		this.playerTwoRequestedDelete = playerTwoRequestedDelete;
	}

	public boolean isPlayerOneReceivedPills() {
		return playerOneReceivedPills;
	}

	public void setPlayerOneReceivedPills(boolean playerOneReceivedPills) {
		this.playerOneReceivedPills = playerOneReceivedPills;
	}

	public boolean isPlayerTwoReceivedPills() {
		return playerTwoReceivedPills;
	}

	public void setPlayerTwoReceivedPills(boolean playerTwoReceivedPills) {
		this.playerTwoReceivedPills = playerTwoReceivedPills;
	}

	public String getChatString() {
		return chatString;
	}

	public void setChatString(String chatString) {
		this.chatString = chatString;
	}

	public void addRejectingPlayer(String username) {

		System.out
				.println("CONTROLLO SU NUMERO DI VOLTE CHE  E STATA RIFIUTATA!!!  "
						+ timesHasBeenRefused);
		if (timesHasBeenRefused == 0)
			firstPlayerRejected = username;
		else if (timesHasBeenRefused == 1)
			secondPlayerRejected = username;
		else if (timesHasBeenRefused == 2)
			thirdPlayerRejected = username;
		else if (timesHasBeenRefused == 3)
			fourthPlayerRejected = username;
		else if (timesHasBeenRefused == 4)
			fifthPlayerRejected = username;

	}

	public boolean hasBeenRejectedByPlayer(String username) {

		if (firstPlayerRejected.equals(username))
			return true;
		else if (secondPlayerRejected.equals(username))
			return true;
		else if (thirdPlayerRejected.equals(username))
			return true;
		else if (fourthPlayerRejected.equals(username))
			return true;
		else if (fifthPlayerRejected.equals(username))
			return true;

		return false;
	}

}
