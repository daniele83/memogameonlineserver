package com.reborngames.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.Gson;

@Entity
@Table(name = "message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int messageLength;
	private String playerName;
	private String cmd;
	private String string1;
	private String string2;
	private String string3;
	private String string4;
	private String string5;
	private String string6;
	private String string7;
	private String string8;
	private String string9;
	private String string10;
	private String string11;
	private String string12;
	private String string13;
	private String string14;
	private String string15;
	private String string16;
	private String string17;
	private String string18;
	private String string19;
	private String string20;
	private String string21;
	private String string22;
	private String string23;
	private String string24;
	private String string25;
	private String string26;
	private String string27;
	private String string28;
	private String string29;
	private String string30;
	

	public Message(String playerName, String[] message) {
		this.playerName = playerName;
		this.messageLength = message.length;
		this.cmd = message[0];
		
		if (messageLength == 31) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
			this.string26 = message[26];
			this.string27 = message[27];
			this.string28 = message[28];
			this.string29 = message[29];
			this.string30 = message[30];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 31 !!!!!");
		}
		else if (messageLength == 30) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
			this.string26 = message[26];
			this.string27 = message[27];
			this.string28 = message[28];
			this.string29 = message[29];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 30 !!!!!");
		}

		else if (messageLength == 29) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
			this.string26 = message[26];
			this.string27 = message[27];
			this.string28 = message[28];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 29 !!!!!");
		}
		else if (messageLength == 28) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
			this.string26 = message[26];
			this.string27 = message[27];
			

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 28 !!!!!");
		}
		else if (messageLength == 27) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
			this.string26 = message[26];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 27 !!!!!");
		}
		else if (messageLength == 26) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
		

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 27 !!!!!");
		}
		else if (messageLength == 26) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];
			this.string25 = message[25];
		

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 26 !!!!!");
		}
		else if (messageLength == 25) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];
			this.string24 = message[24];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 25 !!!!!");
		}
		else if (messageLength == 24) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];
			this.string23 = message[23];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 24 !!!!!");
		}
		else if (messageLength == 23) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];
			this.string22 = message[22];


			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 23 !!!!!");
		}
		else if (messageLength == 22) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];
			this.string21 = message[21];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 22 !!!!!");
		}
		
		else if (messageLength == 21) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
			this.string20 = message[20];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 21 !!!!!");
		}
		else if (messageLength == 20) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
			this.string19 = message[19];
		

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 20 !!!!!");
		}
		else if (messageLength == 19) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
			this.string18 = message[18];
		

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 19 !!!!!");
		}
		else if (messageLength == 18) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
			this.string17 = message[17];
		

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 18 !!!!!");
		}
		else if (messageLength == 17) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];
			this.string16 = message[16];
	
			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 17 !!!!!");
		}
		else if (messageLength == 16) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];
			this.string15 = message[15];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 16 !!!!!");
		} else if (messageLength == 15) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];
			this.string14 = message[14];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 15 !!!!!");
		} else if (messageLength == 14) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];
			this.string13 = message[13];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 14 !!!!!");
		} else if (messageLength == 13) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];
			this.string12 = message[12];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 13 !!!!!");
		} else if (messageLength == 12) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];
			this.string11 = message[11];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 12 !!!!!");
		} else if (messageLength == 11) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];
			this.string10 = message[10];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 11 !!!!!");
		} else if (messageLength == 10) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];
			this.string9 = message[9];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 10 !!!!!");
		} else if (messageLength == 9) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			this.string8 = message[8];

			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 9 !!!!!");
		} else if (messageLength == 8) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			this.string7 = message[7];
			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 8 !!!!!");
		} else if (messageLength == 7) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];
			this.string6 = message[6];
			System.out
					.println("-------------------------CREO MESSAGGIO DI LUNGHEZZA 7 !!!!!");
		} else if (messageLength == 6) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];
			this.string5 = message[5];

		} else if (messageLength == 5) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];
			this.string4 = message[4];

		} else if (messageLength == 4) {
			this.string1 = message[1];
			this.string2 = message[2];
			this.string3 = message[3];

		} else if (messageLength == 3) {
			this.string1 = message[1];
			this.string2 = message[2];

		} else if (messageLength == 2) {
			this.string1 = message[1];
		}

	}

	public Message() {
	}

	public String[] getMessage() {

		String[] message;

		 if (messageLength == 31) {
			message = new String[31];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;
			message[26] = string26;
			message[27] = string27;
			message[28] = string28;
			message[29] = string29;
			message[30] = string30;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 31!!!");

		}
		else if (messageLength ==30) {
			message = new String[30];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;
			message[26] = string26;
			message[27] = string27;
			message[28] = string28;
			message[29] = string29;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 30!!!");

		}
		else if (messageLength == 29) {
			message = new String[29];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;
			message[26] = string26;
			message[27] = string27;
			message[28] = string28;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 29!!!");

		}
		else if (messageLength == 28) {
			message = new String[28];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;
			message[26] = string26;
			message[27] = string27;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 28!!!");

		}
		else if (messageLength == 27) {
			message = new String[27];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;
			message[26] = string26;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 27!!!");

		}
		else if (messageLength == 26) {
			message = new String[26];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;
			message[25] = string25;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 26!!!");

		}
		else if (messageLength == 25) {
			message = new String[25];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;
			message[24] = string24;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 25!!!");

		}
		else if (messageLength == 24) {
			message = new String[24];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;
			message[23] = string23;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 24!!!");

		}
		else if (messageLength == 23) {
			message = new String[23];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;
			message[22] = string22;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 23!!!");

		}
		else if (messageLength == 22) {
			message = new String[22];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;
			message[21] = string21;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 22!!!");

		}
		else if (messageLength == 21) {
			message = new String[21];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			message[20] = string20;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 21!!!");

		}
		else if (messageLength == 20) {
			message = new String[20];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;
			message[19] = string19;
			

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 20!!!");

		}
		else if (messageLength == 19) {
			message = new String[19];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;
			message[18] = string18;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 19!!!");

		}
		else if (messageLength == 18) {
			message = new String[18];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
			message[17] = string17;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 18!!!");

		}
		else if (messageLength == 17) {
			message = new String[17];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;
			message[16] = string16;
		
			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 17!!!");

		}
		else if (messageLength == 16) {
			message = new String[16];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;
			message[15] = string15;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 16!!!");

		} else if (messageLength == 15) {
			message = new String[15];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;
			message[14] = string14;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 15!!!");

		} else if (messageLength == 14) {
			message = new String[14];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;
			message[13] = string13;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 14!!!");

		} else if (messageLength == 13) {
			message = new String[13];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;
			message[12] = string12;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 13!!!");

		} else if (messageLength == 12) {
			message = new String[12];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;
			message[11] = string11;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 12!!!");

		} else if (messageLength == 11) {
			message = new String[11];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			message[10] = string10;

			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 11!!!");

		} else if (messageLength == 10) {
			message = new String[10];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			message[9] = string9;
			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 10!!!");

		} else if (messageLength == 9) {
			message = new String[9];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			message[8] = string8;
			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 9!!!");

		} else if (messageLength == 8) {
			message = new String[8];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			message[7] = string7;
			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 8!!!");

		} else if (messageLength == 7) {
			message = new String[7];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;
			message[6] = string6;
			System.out
					.println("-------------------------RESTITUISCO UN MESSAGGIO DI LUNGHEZZA 7!!!");

		} else if (messageLength == 6) {
			message = new String[6];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;
			message[5] = string5;

		} else if (messageLength == 5) {
			message = new String[5];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;
			message[4] = string4;

		} else if (messageLength == 4) {
			message = new String[4];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;
			message[3] = string3;

		} else if (messageLength == 3) {
			message = new String[3];
			message[0] = cmd;
			message[1] = string1;
			message[2] = string2;

		} else if (messageLength == 2) {
			message = new String[2];
			message[0] = cmd;
			message[1] = string1;

		} else if (messageLength == 1) {
			message = new String[1];
			message[0] = cmd;

		} else {
			message = new String[1];
			message[0] = "???";
		}

		return message;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getMessageLength() {
		return messageLength;
	}

	public void setMessageLength(int messageLength) {
		this.messageLength = messageLength;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getString1() {
		return string1;
	}

	public void setString1(String string1) {
		this.string1 = string1;
	}

	public String getString2() {
		return string2;
	}

	public void setString2(String string2) {
		this.string2 = string2;
	}

	public String getString3() {
		return string3;
	}

	public void setString3(String string3) {
		this.string3 = string3;
	}

	public String getString4() {
		return string4;
	}

	public void setString4(String string4) {
		this.string4 = string4;
	}

	public String getString5() {
		return string5;
	}

	public void setString5(String string5) {
		this.string5 = string5;
	}

	public String getString6() {
		return string6;
	}

	public void setString6(String string6) {
		this.string6 = string6;
	}

	public String getString7() {
		return string7;
	}

	public void setString7(String string7) {
		this.string7 = string7;
	}

	public String getString8() {
		return string8;
	}

	public void setString8(String string8) {
		this.string8 = string8;
	}

	public String getString9() {
		return string9;
	}

	public void setString9(String string9) {
		this.string9 = string9;
	}

	public String getString10() {
		return string10;
	}

	public void setString10(String string10) {
		this.string10 = string10;
	}

	public String getString11() {
		return string11;
	}

	public void setString11(String string11) {
		this.string11 = string11;
	}

	public String getString12() {
		return string12;
	}

	public void setString12(String string12) {
		this.string12 = string12;
	}

	public String getString13() {
		return string13;
	}

	public void setString13(String string13) {
		this.string13 = string13;
	}

	public String getString14() {
		return string14;
	}

	public void setString14(String string14) {
		this.string14 = string14;
	}

	public String getString15() {
		return string15;
	}

	public void setString15(String string15) {
		this.string15 = string15;
	}

	public String getString16() {
		return string16;
	}

	public void setString16(String string16) {
		this.string16 = string16;
	}

	public String getString17() {
		return string17;
	}

	public void setString17(String string17) {
		this.string17 = string17;
	}

	public String getString18() {
		return string18;
	}

	public void setString18(String string18) {
		this.string18 = string18;
	}

	public String getString19() {
		return string19;
	}

	public void setString19(String string19) {
		this.string19 = string19;
	}

	public String getString20() {
		return string20;
	}

	public void setString20(String string20) {
		this.string20 = string20;
	}
	
	

}
