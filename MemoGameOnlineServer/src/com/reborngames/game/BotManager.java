package com.reborngames.game;

import io.netty.channel.Channel;

import java.util.List;
import java.util.Random;

import com.reborngames.entity.Game;
import com.reborngames.entity.User;
import com.reborngames.server.JPAExecutor;

public class BotManager {

	private Lobby lobby;
	private JPAExecutor jpae;

	private Random random = new Random();
	private final static String characters = "abcdefghijklmnopqrstuvwxyz";

	public BotManager(Lobby lobby, JPAExecutor jpae) {
		this.lobby = lobby;
		this.jpae = jpae;

	}

	public User allocateBot(Long gameId, String opponentName, int opponentLevel) {

		boolean botFound = false;
		List<User> botsList = jpae.botsLevelBasedLookUp(
				opponentLevel - Lobby.getMatchmakingRange() / 2, opponentLevel
						+ Lobby.getMatchmakingRange() / 2);

		User bot = null;
		if (botsList.size() > 0) {
			System.out
					.println("BOT MANAGER: Ho trovato la seguente lista di bot compatibili: ");
			System.out.println("DIMENSIONI: " + botsList.size());
			for (User u : botsList)
				System.out.println("Nome Bot: " + u.getUsername()
						+ " e livello: " + u.getLevel());
			Random r = new Random();
			int index = r.nextInt(botsList.size());
			bot = botsList.get(index);
			if (!jpae.gameAlreadyExists(opponentName, bot.getUsername())) {
				botFound = true;
				incrementOpenGames(bot);

			} else {// TODO riciclare nell'array, affinare metodo ricerca e poi
					// istanziare nuovo bot
				System.out.println("Partita gia presente tra  i due player!");
			}

			// TODO anche se la lista NON e vuota potrebbe essere necessario cmq
			// istanziare
			// un bot (magari perche hanno giocato fin troppe aprtite tra di
			// loro )
		}
		if (!botFound) {
			bot = createBootUser(opponentLevel);
			System.out
					.println("BOT MANAGER: Nessun bot compatibile, ne creo uno nuovo!!!");
		}

		setPlayerTwoIntoGame(gameId, bot.getUsername());

		return bot;

	}

	public User createBootUser(int opponentLevel) {

		String username = jpae.botNameGenerate();
		if (username == null)
			username = this.generateUserName();

		int level = (opponentLevel - Lobby.getMatchmakingRange() / 2)
				+ random.nextInt(Lobby.getMatchmakingRange());
		while (level <= 0) {
			level = (opponentLevel - Lobby.getMatchmakingRange() / 2)
					+ random.nextInt(Lobby.getMatchmakingRange());
		}
		int imageId = 0;

		if (level <= 12)
			imageId = random.nextInt(12);
		else if (level >= 12 && level <= 15)
			imageId = random.nextInt(16);
		else if (level >= 16 && level <= 19)
			imageId = random.nextInt(20);
		else if (level >= 20 && level <= 23)
			imageId = random.nextInt(24);
		else if (level >= 24 && level <= 27)
			imageId = random.nextInt(28);
		else if (level >= 28 && level <= 31)
			imageId = random.nextInt(32);
		else if (level >= 32 && level <= 35)
			imageId = random.nextInt(36);
		else if (level >= 36 && level <= 39)
			imageId = random.nextInt(40);
		else if (level >= 40 && level <= 43)
			imageId = random.nextInt(44);
		else if (level >= 44)
			imageId = random.nextInt(48);

		User user = new User();
		user.setBot(true);
		user.setUsername(username);
		user.setFacebookId("");
		user.setFacebookPictureURL("");
		user.setImage(imageId);
		user.setLevel(level);
		user.setExp(0);
		user.setPills(0);
		// istanzio un bot quando serve per una partita
		user.setOpenGames(1);
		// TODO settare le aprtite in maniera coerente con il livello, devono
		// essere credibili sti boot!

		if (level > 1) {

			user.setPlayedGames(10 * level + random.nextInt(level * 4));
			user.setGivenUpMatches(0);
			user.setWonGames(35 + random.nextInt(35));

			user.setConsecutiveMatchWon(5 + random.nextInt(15));

			if (user.getConsecutiveMatchWon() > user.getWonGames())
				user.setConsecutiveMatchWon(user.getWonGames() - 4);
			user.setTempConsecutiveMatchesWon(0);
			user.setHighestmatchScore(900 + random.nextInt(400));
			user.setHighestFirstRoundScore(900 + random.nextInt(400));
			user.setBestFirstRoundTime(4 + random.nextFloat() * 5);
			user.setHighestSecondRoundScore(900 + random.nextInt(400));
			user.setBestSecondRoundTime(4 + random.nextFloat() * 5);
			user.setHighestThirdRoundScore(900 + random.nextInt(400));
			user.setBestThirdRoundTime(0);

		} else {
			user.setPlayedGames(0);
			user.setGivenUpMatches(0);
			user.setWonGames(0);

			user.setConsecutiveMatchWon(0);
			user.setTempConsecutiveMatchesWon(0);
			user.setHighestmatchScore(0);
			user.setHighestFirstRoundScore(0);
			user.setBestFirstRoundTime(0);
			user.setHighestSecondRoundScore(0);
			user.setBestSecondRoundTime(0);
			user.setHighestThirdRoundScore(0);
			user.setBestThirdRoundTime(0);
		}

		user.setFirstRoundCounter(0);
		user.setFirstRoundSum(0l);
		user.setSecondRoundCounter(0);
		user.setSecondRoundSum(0l);
		user.setThirdRoundCounter(0);
		user.setThirdRoundSum(0l);

		jpae.userCreate(user);
		System.out
				.println("- - BOT CREATOR: creato nuovo BOT con i seguenti dati:");
		System.out.println("- - Username: " + user.getUsername());
		System.out.println("- - Level: " + user.getLevel());
		System.out.println("- - Image ID: " + user.getImage());

		return user;

	}

	private String generateUserName() {

		char[] text = new char[6];
		for (int i = 0; i < 6; i++) {
			text[i] = characters.charAt(random.nextInt(characters.length()));
		}
		return new String(text);

	}

	private void setPlayerTwoIntoGame(Long idGame, String bootName) {

		jpae.getEm().getTransaction().begin();
		Game game = jpae.getEm().find(Game.class, idGame);
		game.setPlayerTwo(bootName);
		jpae.getEm().flush();
		jpae.getEm().getTransaction().commit();
		System.out
				.println("BOT MANAGER: setto il bot come platerTwo della partita");
	}

	public void incrementOpenGames(User bot) {

		jpae.getEm().getTransaction().begin();
		User user = jpae.botLookUp(bot.getUsername());
		user.setOpenGames(user.getOpenGames() + 1);
		System.out
				.println("Incremento il numero di partite ATTIVE! ora uguali a "
						+ user.getOpenGames()
						+ " per il BOT "
						+ user.getUsername());
		jpae.getEm().flush();
		jpae.getEm().getTransaction().commit();

	}

	public Integer generateFirstRoundRandomScore(User user) {

		if (user.getFirstRoundCounter() > 0 && user.getFirstRoundSum() > 0) {
			float avarageUserScore = (float) (user.getFirstRoundSum() / user
					.getFirstRoundCounter());

			float randomGeneratorRangeCoefficinet = 0.30f;
			float startingPoint = 0.72f;

			if (user.getLevel() >= 4 && user.getLevel() <= 7) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.73f;
			} else if (user.getLevel() >= 8 && user.getLevel() <= 11) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.74f;
			} else if (user.getLevel() >= 12 && user.getLevel() <= 15) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.75f;
			} else if (user.getLevel() >= 16 && user.getLevel() <= 19) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.76f;
			} else if (user.getLevel() >= 20 && user.getLevel() <= 23) {
				randomGeneratorRangeCoefficinet = 0.29f;
				startingPoint = 0.77f;
			} else if (user.getLevel() >= 24 && user.getLevel() <= 27) {
				randomGeneratorRangeCoefficinet = 0.28f;
				startingPoint = 0.78f;
			} else if (user.getLevel() >= 28 && user.getLevel() <= 31) {
				randomGeneratorRangeCoefficinet = 0.27f;
				startingPoint = 0.79f;
			} else if (user.getLevel() >= 32 && user.getLevel() <= 35) {
				randomGeneratorRangeCoefficinet = 0.26f;
				startingPoint = 0.80f;
			} else if (user.getLevel() >= 36 && user.getLevel() <= 39) {
				randomGeneratorRangeCoefficinet = 0.25f;
				startingPoint = 0.81f;
			} else if (user.getLevel() >= 40 && user.getLevel() <= 43) {
				randomGeneratorRangeCoefficinet = 0.24f;
				startingPoint = 0.82f;
			} else if (user.getLevel() >= 44 && user.getLevel() <= 47) {
				randomGeneratorRangeCoefficinet = 0.23f;
				startingPoint = 0.83f;
			} else if (user.getLevel() >= 50) {
				randomGeneratorRangeCoefficinet = 0.22f;
				startingPoint = 0.84f;
			}

			int randomGeneratorRange = (int) (avarageUserScore * randomGeneratorRangeCoefficinet);
			float score = (avarageUserScore * startingPoint)
					+ random.nextInt(randomGeneratorRange);

			System.out.println("Parto da una media di: " + avarageUserScore);
			System.out.println("Ho una base di: "
					+ (avarageUserScore - (avarageUserScore * 0.24f)));
			System.out.println("Genero intero di range: "
					+ randomGeneratorRange);

			if (score > 0)
				return (int) score;
			else
				return 700 + random.nextInt(300);

		} else {
			return 800 + random.nextInt(300);
		}
	}

	public Integer generateSecondRoundRandomScore(User user) {
		if (user.getSecondRoundCounter() > 0 && user.getSecondRoundSum() > 0) {
			float avarageUserScore = (float) (user.getSecondRoundSum() / user
					.getSecondRoundCounter());

			float randomGeneratorRangeCoefficinet = 0.30f;
			float startingPoint = 0.72f;

			if (user.getLevel() >= 4 && user.getLevel() <= 7) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.73f;
			} else if (user.getLevel() >= 8 && user.getLevel() <= 11) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.74f;
			} else if (user.getLevel() >= 12 && user.getLevel() <= 15) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.75f;
			} else if (user.getLevel() >= 16 && user.getLevel() <= 19) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.76f;
			} else if (user.getLevel() >= 20 && user.getLevel() <= 23) {
				randomGeneratorRangeCoefficinet = 0.29f;
				startingPoint = 0.77f;
			} else if (user.getLevel() >= 24 && user.getLevel() <= 27) {
				randomGeneratorRangeCoefficinet = 0.28f;
				startingPoint = 0.78f;
			} else if (user.getLevel() >= 28 && user.getLevel() <= 31) {
				randomGeneratorRangeCoefficinet = 0.27f;
				startingPoint = 0.79f;
			} else if (user.getLevel() >= 32 && user.getLevel() <= 35) {
				randomGeneratorRangeCoefficinet = 0.26f;
				startingPoint = 0.80f;
			} else if (user.getLevel() >= 36 && user.getLevel() <= 39) {
				randomGeneratorRangeCoefficinet = 0.25f;
				startingPoint = 0.81f;
			} else if (user.getLevel() >= 40 && user.getLevel() <= 43) {
				randomGeneratorRangeCoefficinet = 0.24f;
				startingPoint = 0.82f;
			} else if (user.getLevel() >= 44 && user.getLevel() <= 47) {
				randomGeneratorRangeCoefficinet = 0.23f;
				startingPoint = 0.83f;
			} else if (user.getLevel() >= 50) {
				randomGeneratorRangeCoefficinet = 0.22f;
				startingPoint = 0.84f;
			}

			int randomGeneratorRange = (int) (avarageUserScore * randomGeneratorRangeCoefficinet);
			float score = (avarageUserScore * startingPoint)
					+ random.nextInt(randomGeneratorRange);

			System.out.println("Parto da una media di: " + avarageUserScore);
			System.out.println("Ho una base di: "
					+ (avarageUserScore - (avarageUserScore * 0.24f)));
			System.out.println("Genero intero di range: "
					+ randomGeneratorRange);

			if (score > 0)
				return (int) score;
			else
				return 750 + random.nextInt(300);

		} else {
			return 800 + random.nextInt(300);
		}
	}

	public Integer generateThirdRoundRandomScore(User user) {
		if (user.getThirdRoundCounter() > 0 && user.getThirdRoundSum() > 0) {
			float avarageUserScore = (float) (user.getThirdRoundSum() / user
					.getThirdRoundCounter());

			float randomGeneratorRangeCoefficinet = 0.30f;
			float startingPoint = 0.72f;

			if (user.getLevel() >= 4 && user.getLevel() <= 7) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.73f;
			} else if (user.getLevel() >= 8 && user.getLevel() <= 11) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.74f;
			} else if (user.getLevel() >= 12 && user.getLevel() <= 15) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.75f;
			} else if (user.getLevel() >= 16 && user.getLevel() <= 19) {
				randomGeneratorRangeCoefficinet = 0.30f;
				startingPoint = 0.76f;
			} else if (user.getLevel() >= 20 && user.getLevel() <= 23) {
				randomGeneratorRangeCoefficinet = 0.29f;
				startingPoint = 0.77f;
			} else if (user.getLevel() >= 24 && user.getLevel() <= 27) {
				randomGeneratorRangeCoefficinet = 0.28f;
				startingPoint = 0.78f;
			} else if (user.getLevel() >= 28 && user.getLevel() <= 31) {
				randomGeneratorRangeCoefficinet = 0.27f;
				startingPoint = 0.79f;
			} else if (user.getLevel() >= 32 && user.getLevel() <= 35) {
				randomGeneratorRangeCoefficinet = 0.26f;
				startingPoint = 0.80f;
			} else if (user.getLevel() >= 36 && user.getLevel() <= 39) {
				randomGeneratorRangeCoefficinet = 0.25f;
				startingPoint = 0.81f;
			} else if (user.getLevel() >= 40 && user.getLevel() <= 43) {
				randomGeneratorRangeCoefficinet = 0.24f;
				startingPoint = 0.82f;
			} else if (user.getLevel() >= 44 && user.getLevel() <= 47) {
				randomGeneratorRangeCoefficinet = 0.23f;
				startingPoint = 0.83f;
			} else if (user.getLevel() >= 50) {
				randomGeneratorRangeCoefficinet = 0.22f;
				startingPoint = 0.84f;
			}

			int randomGeneratorRange = (int) (avarageUserScore * randomGeneratorRangeCoefficinet);
			float score = (avarageUserScore * startingPoint)
					+ random.nextInt(randomGeneratorRange);

			System.out.println("Parto da una media di: " + avarageUserScore);
			System.out.println("Ho una base di: "
					+ (avarageUserScore - (avarageUserScore * 0.24f)));
			System.out.println("Genero intero di range: "
					+ randomGeneratorRange);

			if (score > 0)
				return (int) score;
			else
				return 750 + random.nextInt(300);

		} else {
			return 800 + random.nextInt(300);
		}
	}
}
