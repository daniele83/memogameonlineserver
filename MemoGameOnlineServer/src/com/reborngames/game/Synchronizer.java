package com.reborngames.game;

import com.reborngames.server.JPAExecutor;

public class Synchronizer {

	private Lobby lobby;
	private JPAExecutor jpae;

	public Synchronizer(Lobby lobby, JPAExecutor jpae) {
		this.lobby = lobby;
		this.jpae = jpae;
	}
}
