package com.reborngames.game;

import io.netty.channel.Channel;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import io.netty.util.concurrent.ScheduledFuture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.reborngames.entity.BotName;
import com.reborngames.entity.Game;
import com.reborngames.entity.Message;
import com.reborngames.entity.User;
import com.reborngames.server.JPAExecutor;

/**
 * Classe decoupled per MOGameServerAssistant
 * 
 * void: C - connected 0100: S - connected ack 0105: C - signin attempt +
 * username + password 0106: S - signin TRUE - viene reindirizzato ad HOME 0107:
 * S - signin FALSE - viene reindirzzato ad SignUp
 * 
 * @author Emanuele
 * 
 */
public class Lobby {

	private JPAExecutor jpae;
	private BotManager botManager;
	private Synchronizer sync;

	public ArrayList<Player> playerList = new ArrayList<Player>();
	public Map<Channel, Player> playerMap = new HashMap<>();
	public Map<Long, Message> messageMap = new HashMap<>();
	private Long messageIDCounter = 0l;
	public LevelManager levelManager = new LevelManager();

	HashedWheelTimer scheduler = new HashedWheelTimer();

	Gson jFields = new Gson();
	private final static String[] syncMessage = { "9991" };

	// public List<Game> gameList = new ArrayList<Game>();
	// public Map<Long, Game> gameMap = new HashMap<>();

	// private int idGame;

	private final static int timeToResendMessage = 25;// seconds
	private final static int timeForSynchronization = 5;// seconds
	private final static int matchMakingRange = 5;
	private final static int maxTimesGameCanBeRejected = 4;
	private final static int timeToAutomaticRefuse = 25;

	private final static Random random = new Random();

	private final ScheduledExecutorService executor = Executors
			.newScheduledThreadPool(1);

	public Lobby() {
		// idGame = idGameInit();
	}

	public Lobby(JPAExecutor jpae) {
		this.jpae = jpae;
		this.botManager = new BotManager(this, jpae);
		this.sync = new Synchronizer(this, jpae);
		// jpae.resetDB();
		this.addNameToBotList();

	}

	/**
	 * @param String
	 *            cmd
	 * @param String
	 *            [] msgRcv - il primo address dell'array e occupato dal cmd
	 * @return
	 */
	public void servePlayer(String cmd, String[] msgRcv, Channel channel) {

		switch (cmd) {
		// SYNCH MSG
		case "9990":
			channel.writeAndFlush(jFields.toJson(syncMessage) + "\r\n");
			break;
		// EMPTY MESSAGE STACK
		case "9992":
			emptyMessageStack(channel);
			break;
		// MESSAGE ACK
		case "9993":
			receivedACK(Long.valueOf(msgRcv[1]), channel);
			break;
		// SIGNUP
		case "0102":
			signUp(msgRcv[1], msgRcv[2], msgRcv[3], msgRcv[4], channel);
			break;
		// SIGNIN
		case "0105":
			signIn(msgRcv[1], msgRcv[2], channel);
			break;
		// SIGNOUT
		case "0109":
			signOut(channel);
			break;
		// SIGNOUT
		case "0113":
			signUpFacebook(msgRcv[1], msgRcv[2], msgRcv[3], msgRcv[4],
					msgRcv[5], channel);
			break;
		// FB SIGNIN/SIGNUP
		case "0115":
			signInFacebook(msgRcv[1], channel);
			break;
		// RECOVER PASSWORD
		case "0120":
			recoverPassword(msgRcv, channel);
			break;
		// AVVERSARIO CASUALE
		case "0200":
			randomOpponentChallenge(channel);
			break;
		// SFIDA CASUALE RIFIUTATA
		case "0207":
			randomChallengeRefused(msgRcv, channel, false, null);
			break;
		// SFIDA CASUALE ACCETTATA
		case "0208":
			randomChallengeAccepted(msgRcv, channel);
			break;
		// SFIDA UN AMICO
		case "0300":
			challengeFriend(msgRcv[1], channel);
			break;
		// L'AMICO RIFIUTA
		case "0303":
			friendChallengeRefused(msgRcv[1], channel);
			break;
		// CHANGE EMAIL
		case "0600":
			changeEmail(channel, msgRcv[1]);
			break;
		// CHANGE PASSWORD
		case "0601":
			changePassword(channel, msgRcv[1]);
			break;
		// CHANGE IMAGE ID
		case "0602":
			changeImageId(channel, msgRcv[1]);
			break;
		// SET FACEBOOK INFO
		case "0603":
			setFacebookInfo(channel, msgRcv[1], msgRcv[2]);
			break;
		// incrementPowerUpTime
		case "0604":
			this.incrementPowerUpTime(channel, msgRcv);
			break;
		// incrementPowerUpDoubleCouple
		case "0605":
			this.incrementPowerUpDoubleCouple(channel, msgRcv);
			break;
		// incrementPowerUpFaceUp
		case "0606":
			this.incrementPowerUpFaceUp(channel, msgRcv);
			break;
		// incrementPowerUpStop
		case "0607":
			this.incrementPowerUpStop(channel, msgRcv);
			break;
		// incrementPowerUpChain
		case "0608":
			this.incrementPowerUpChain(channel, msgRcv);
			break;
		// incrementPowerUpBonus
		case "0609":
			this.incrementPowerUpBonus(channel, msgRcv);
			break;
		// decrementPowerUpTime
		case "0610":
			this.decrementPowerUpTime(channel);
			break;
		// decrementPowerUpDoubleCouple
		case "0611":
			this.decrementPowerUpDoubleCouple(channel);
			break;
		// decrementPowerUpFaceUp
		case "0612":
			this.decrementPowerUpFaceUp(channel);
			break;
		// decrementPowerUpStop
		case "0613":
			this.decrementPowerUpStop(channel);
			break;
		// decrementPowerUpChain
		case "0614":
			this.decrementPowerUpChain(channel);
			break;
		// decrementPowerUpBonus
		case "0615":
			this.decrementPowerUpBonus(channel);
			break;
		// decrementPowerUpBonus
		case "0616":
			this.decrementPills(channel, msgRcv);
			break;
		// RICHIESTA SYNC
		case "0800":
			synchronizePlayer(channel);
			break;
		// ABBANDONA MATCH
		case "1000":
			abbandonGame(msgRcv, channel);
			break;
		// CANCELLA MATCH
		case "1003":
			deleteGame(msgRcv, channel);
			break;
		// ROUND END + GAME END
		case "1005":
			roundEnd(msgRcv, channel);
			break;
		// PILLS and XP BONUS RECEIVED
		case "1008":
			pillsAndXpBonusReceived(msgRcv, channel);
			break;
		// EXISTENCE OF PLAYER (AND IMAGE ID)
		case "0900":
			checkPlayerAndInfo(msgRcv[1], channel);
			break;
		// CHAT MESSAGE
		case "2000":
			sendChatMessage(msgRcv, channel);
			break;
		}
	}

	private void abbandonGame(String[] msgRcv, Channel channel) {
		Game game = jpae.gameLookUp(Long.valueOf(msgRcv[1]));
		String playerWhoGaveUp = playerMap.get(channel).getUserData()
				.getUsername();
		String playerForcedToCloseMatch = this.returnOpponentName(
				Long.valueOf(msgRcv[1]), playerWhoGaveUp);

		String[] ack = { "1001", msgRcv[1] };
		sendToPlayer(playerWhoGaveUp, ack);

		String[] msg = { "1002", msgRcv[1] };
		sendToPlayer(playerForcedToCloseMatch, msg);

		this.manageGivenUpGames(game, playerWhoGaveUp, playerForcedToCloseMatch);

	}

	public void signIn(String username, String password, final Channel channel) {
		String[] result = { "0107", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "" };// 0107 SUCCESS 0108
															// FAIL
		// this.messageTest();
		System.out.println("- - Lobby Sign In");
		System.out.println("- - USERNAME: " + username + " , PASSWORD: "
				+ password);

		// Verifico user non gia loggato (caso possibile nell'eventualita di un
		// secondo login da un altro dispositivo)
		System.out.println("- - Players gia connessi: ");
		for (Player p : playerMap.values()) {
			System.out.println("- - " + p.getUserData().getUsername());
			if (p.getUserData().getUsername().equals(username)) {
				// Player gia loggato da un differente device
				System.out.println("- - Il player " + username
						+ " a gia loggato!");
				result[0] = "0108";
				System.out.println("- - Lobby Result: " + result[0]);
				break;
			}
		}

		System.out.println("- - -\n- - -");

		if (result[0].equals("0107")) {
			User u = jpae.generalUserLookUp(username, password);
			if (u != null) {
				final Player p = new Player(u, channel);
				// TODO Il Server deve passare al Client le partite in corso
				List<Game> openGameList = jpae.getGameList(u.getUsername());
				playerList.add(p);
				playerMap.put(channel, p);

				/*
				 * channel.pipeline().addFirst("idleStateHandler", new
				 * IdleStateHandler(5, 0, 0));
				 */

				result[0] = "0106";
				result[1] = String.valueOf(u.getFacebookPictureURL());
				result[2] = String.valueOf(u.getLevel());
				result[3] = String.valueOf(u.getExp());
				result[4] = String.valueOf(u.getImage());
				result[5] = String.valueOf(u.getPlayedGames());
				result[6] = String.valueOf(u.getWonGames());
				result[7] = String.valueOf(u.getConsecutiveMatchWon());
				result[8] = String.valueOf(u.getHighestmatchScore());
				result[9] = String.valueOf(u.getHighestFirstRoundScore());
				result[10] = String.valueOf(u.getBestFirstRoundTime());
				result[11] = String.valueOf(u.getHighestSecondRoundScore());
				result[12] = String.valueOf(u.getBestSecondRoundTime());
				result[13] = String.valueOf(u.getHighestThirdRoundScore());
				result[14] = String.valueOf(u.getBestThirdRoundTime());
				result[15] = String.valueOf(u.getPowerUp_Time());
				result[16] = String.valueOf(u.getPowerUp_DoubleCouple());
				result[17] = String.valueOf(u.getPowerUp_FaceUp());
				result[18] = String.valueOf(u.getPowerUp_Stop());
				result[19] = String.valueOf(u.getPowerUp_Chain());
				result[20] = String.valueOf(u.getPowerUp_Bonus());
				result[21] = String.valueOf(u.getPills());

				System.out
						.println("- - SIGN IN Loggato PLAYER con i seguenti dati:");
				System.out.println("- - Username: " + u.getUsername());
				System.out.println("- - Password: " + u.getPassword());
				System.out.println("- - Facebook ID: " + u.getFacebookId());
				System.out.println("- - Email: " + u.getEmail());
				System.out.println("- - Image ID: " + u.getImage());
				System.out.println("- - Partite  in corso: "
						+ openGameList.size());

			}
		}
		if (result[0].equals("0106"))
			this.sendToPlayer(username, result);
		else
			this.sendToPlayer(channel, result);
	}

	public void signInFacebook(String facebookId, Channel channel) {
		String[] result = { "0111", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "" };// 0111 SUCCESS
																	// 0108 FAIL

		// this.messageTest();
		System.out.println("- - Lobby Sign In Facebook");
		System.out.println("- - FaceBook_ID: " + facebookId);

		// Verifico user non gia loggato (caso possibile nell'eventualita di un
		// secondo login da un nuovo dispositivo)
		System.out.println("- - Players gia connessi: ");
		for (Player p : playerMap.values()) {
			System.out.println("- - " + p.getUserData().getFacebookId());
			if (p.getUserData().getFacebookId().equals(facebookId)) {
				// Player gia loggato da un differente device
				System.out.println("- - Il player con facebookID" + facebookId
						+ " e gia loggato!");
				result[0] = "0108";
				System.out.println("- - Lobby Result: " + result[0]);
				break;
			}
		}

		System.out.println("- - -\n- - -");

		if (result[0].equals("0111")) {
			User u = jpae.userLookUpFacebookId(facebookId);
			if (u != null) {// Player presente nel DB con facebookId
				Player p = new Player(u, channel);
				// TODO Il Server deve passare al Client le partite in corso
				List<Game> openGameList = jpae.getGameList(u.getUsername());
				playerList.add(p);
				playerMap.put(channel, p);
				result[1] = u.getUsername();
				result[2] = u.getFacebookPictureURL();
				result[3] = String.valueOf(u.getLevel());
				result[4] = String.valueOf(u.getExp());
				result[5] = String.valueOf(u.getImage());
				result[6] = String.valueOf(u.getPlayedGames());
				result[7] = String.valueOf(u.getWonGames());
				result[8] = String.valueOf(u.getConsecutiveMatchWon());
				result[9] = String.valueOf(u.getHighestmatchScore());
				result[10] = String.valueOf(u.getHighestFirstRoundScore());
				result[11] = String.valueOf(u.getBestFirstRoundTime());
				result[12] = String.valueOf(u.getHighestSecondRoundScore());
				result[13] = String.valueOf(u.getBestSecondRoundTime());
				result[14] = String.valueOf(u.getHighestThirdRoundScore());
				result[15] = String.valueOf(u.getBestThirdRoundTime());
				result[16] = String.valueOf(u.getPowerUp_Time());
				result[17] = String.valueOf(u.getPowerUp_DoubleCouple());
				result[18] = String.valueOf(u.getPowerUp_FaceUp());
				result[19] = String.valueOf(u.getPowerUp_Stop());
				result[20] = String.valueOf(u.getPowerUp_Chain());
				result[21] = String.valueOf(u.getPowerUp_Bonus());
				result[22] = String.valueOf(u.getPills());

				System.out
						.println("- - SIGN IN Loggato PLAYER con i seguenti dati:");
				System.out.println("- - Username: " + u.getUsername());
				System.out.println("- - Password: " + u.getPassword());
				System.out.println("- - Facebook ID: " + u.getFacebookId());
				System.out.println("- - Email: " + u.getEmail());
				System.out.println("- - Image ID: " + u.getImage());
				System.out.println("- - Partite  in corso: "
						+ openGameList.size());

			} else {// Player NON presente nel DB con questo
					// facebookId,reindirizzo il player al SIGN UP CON FB!!!
				result[0] = "0112";
			}
		}
		if (result[0].equals("0111"))
			this.sendToPlayer(playerMap.get(channel).getUserData()
					.getUsername(), result);
		else
			this.sendToPlayer(channel, result);
	}

	public void signUp(String username, String password, String email,
			String image, Channel channel) {

		String[] result = { "0104" };

		System.out.println("ENTRO IN SIGN UP!!!  ");

		if (jpae.generalUserLookUp(username) == null) {
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setFacebookId("");
			user.setFacebookPictureURL("");
			user.setEmail(email);
			user.setImage(Integer.valueOf(image));
			user.setLevel(1);
			user.setExp(1);
			user.setPills(0);
			user.setBot(false);
			user.setOpenGames(0);
			user.setPlayedGames(0);
			user.setGivenUpMatches(0);
			user.setWonGames(0);

			user.setPowerUp_Bonus(0);
			user.setPowerUp_Chain(0);
			user.setPowerUp_DoubleCouple(0);
			user.setPowerUp_FaceUp(0);
			user.setPowerUp_Stop(0);
			user.setPowerUp_Time(0);

			user.setConsecutiveMatchWon(0);
			user.setTempConsecutiveMatchesWon(0);
			user.setHighestmatchScore(0);
			user.setHighestFirstRoundScore(0);
			user.setBestFirstRoundTime(0);
			user.setHighestSecondRoundScore(0);
			user.setBestSecondRoundTime(0);
			user.setHighestThirdRoundScore(0);
			user.setBestThirdRoundTime(0);

			user.setFirstRoundCounter(0);
			user.setFirstRoundSum(0l);
			user.setSecondRoundCounter(0);
			user.setSecondRoundSum(0l);
			user.setThirdRoundCounter(0);
			user.setThirdRoundSum(0l);

			jpae.userCreate(user);
			Player p = new Player(user, channel);
			playerList.add(p);
			playerMap.put(channel, p);
			result[0] = "0103";
			System.out
					.println("- - SIGN UP Creato nuovo account con i seguenti dati:");
			System.out.println("- - Username: " + user.getUsername());
			System.out.println("- - Password: " + user.getPassword());
			System.out.println("- - Facebook ID: " + user.getFacebookId());
			System.out.println("- - Facebook picture URL: "
					+ user.getFacebookPictureURL());
			System.out.println("- - Email: " + user.getEmail());
			System.out.println("- - Image ID: " + user.getImage());

			// genero anche nuovo bot
			// botF.createBot();

		} else {
			result[0] = "0101";
		}

		if (result[0].equals("0103"))
			this.sendToPlayer(username, result);
		else
			this.sendToPlayer(channel, result);
	}

	public void signUpFacebook(String username, String facebookID,
			String facebookPictureURL, String email, String image,
			Channel channel) {
		String[] result = { "0104" };

		if (jpae.generalUserLookUp(username) == null) {
			User user = new User();
			user.setUsername(username);
			user.setPassword("");
			user.setFacebookId(facebookID);
			user.setFacebookPictureURL(facebookPictureURL);
			user.setEmail(email);
			user.setImage(Integer.valueOf(image));
			user.setLevel(1);
			user.setExp(1);
			user.setPills(0);
			user.setBot(false);
			user.setOpenGames(0);
			user.setPlayedGames(0);
			user.setGivenUpMatches(0);
			user.setWonGames(0);

			user.setPowerUp_Bonus(0);
			user.setPowerUp_Chain(0);
			user.setPowerUp_DoubleCouple(0);
			user.setPowerUp_FaceUp(0);
			user.setPowerUp_Stop(0);
			user.setPowerUp_Time(0);

			user.setConsecutiveMatchWon(0);
			user.setTempConsecutiveMatchesWon(0);
			user.setHighestmatchScore(0);
			user.setHighestFirstRoundScore(0);
			user.setBestFirstRoundTime(0);
			user.setHighestSecondRoundScore(0);
			user.setBestSecondRoundTime(0);
			user.setHighestThirdRoundScore(0);
			user.setBestThirdRoundTime(0);

			user.setFirstRoundCounter(0);
			user.setFirstRoundSum(0l);
			user.setSecondRoundCounter(0);
			user.setSecondRoundSum(0l);
			user.setThirdRoundCounter(0);
			user.setThirdRoundSum(0l);

			jpae.userCreate(user);
			Player p = new Player(user, channel);
			playerList.add(p);
			playerMap.put(channel, p);
			result[0] = "0103";
			System.out
					.println("- - SIGN UP Creato nuovo account con i seguenti dati:");
			System.out.println("- - Username: " + user.getUsername());
			System.out.println("- - Password: " + user.getPassword());
			System.out.println("- - Facebook ID: " + user.getFacebookId());
			System.out.println("- - Facebook Picture URL: "
					+ user.getFacebookPictureURL());
			System.out.println("- - Email: " + user.getEmail());
			System.out.println("- - Image ID: " + user.getImage());

		} else {
			result[0] = "0101";
		}

		if (result[0].equals("0103"))
			this.sendToPlayer(username, result);
		else
			this.sendToPlayer(channel, result);
	}

	private void signOut(Channel channel) {

		playerList.remove(playerMap.get(channel));
		playerMap.remove(channel);
	}

	/**
	 * LO STATO DI "ATTESA RANDOM PLAYER" LATO CLIENT SARA' IMPOSTO DAL CLIENT
	 * STESSO DOPO AVER INVIATO IL MESSAGGIO 0200
	 * 
	 * @param channel
	 * @return String[]
	 */
	private void randomOpponentChallenge(Channel channel) {

		System.out.println("- - Lobby Random Opponent");
		String[] result = { "0211", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "" };
		User challengingPlayer = playerMap.get(channel).getUserData();
		boolean newHumanOpponentFound = false;
		Game game = new Game(challengingPlayer.getUsername());
		game.setTimesHasBeenRefused(0);
		game.setClosed(false);
		incrementOpenGames(playerMap.get(channel));
		// ///PERSISTENZA
		jpae.gameCreate(game);
		// ///Persistenza

		Long gameId = game.getId();

		// gameList.add(game);
		// gameMap.put(idGame, game);
		System.out.println("- - Creato Game con id: " + game.getId());
		// idGame++;

		// itera in playerList cercando giocatori di livello simile e con poche
		// partite aperte. Controlla inoltre che non ci siano altre partite
		// attivate tra gli stessi player
		label: for (Player challengedPlayer : playerList) {
			/*
			 * if (challengingPlayer.playerCh != p.playerCh && p.getOpenGames()
			 * < 10 && (challengingPlayer.getLevel() >= (p.getLevel() - 5) &&
			 * challengingPlayer .getLevel() <= (p.getLevel() + 5))) {
			 */

			System.out.println("CHECK SU "
					+ challengedPlayer.getUserData().getUsername());
			System.out
					.println(challengingPlayer.getUsername() != challengedPlayer
							.getUserData().getUsername());

			System.out
					.println(challengedPlayer.getUserData().getLevel() >= challengingPlayer
							.getLevel() - matchMakingRange / 2);
			System.out
					.println(challengedPlayer.getUserData().getLevel() <= challengingPlayer
							.getLevel() + matchMakingRange / 2);
			System.out.println(!jpae.gameAlreadyExists(challengingPlayer
					.getUsername(), challengedPlayer.getUserData()
					.getUsername()));
			System.out
					.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");

			if (challengingPlayer.getUsername() != challengedPlayer
					.getUserData().getUsername()
					&& challengedPlayer.getUserData().getLevel() >= challengingPlayer
							.getLevel() - matchMakingRange / 2
					&& challengedPlayer.getUserData().getLevel() <= challengingPlayer
							.getLevel() + matchMakingRange / 2
					&& !jpae.gameAlreadyExists(challengingPlayer.getUsername(),
							challengedPlayer.getUserData().getUsername())) {
				result[0] = "0206";
				result[1] = String.valueOf(gameId);
				result[2] = String.valueOf(challengingPlayer.getUsername());
				result[3] = String.valueOf(challengingPlayer.getLevel());
				result[4] = String.valueOf(challengingPlayer.getImage());
				result[5] = String.valueOf(challengingPlayer
						.getFacebookPictureURL());
				result[6] = String.valueOf(challengingPlayer.getPlayedGames());
				result[7] = String.valueOf(challengingPlayer.getWonGames());
				result[8] = String.valueOf(challengingPlayer
						.getConsecutiveMatchWon());
				result[9] = String.valueOf(challengingPlayer
						.getHighestmatchScore());
				result[10] = String.valueOf(challengingPlayer
						.getHighestFirstRoundScore());
				result[11] = String.valueOf(challengingPlayer
						.getBestFirstRoundTime());
				result[12] = String.valueOf(challengingPlayer
						.getHighestSecondRoundScore());
				result[13] = String.valueOf(challengingPlayer
						.getBestSecondRoundTime());
				result[14] = String.valueOf(challengingPlayer
						.getHighestThirdRoundScore());
				result[15] = String.valueOf(challengingPlayer
						.getBestThirdRoundTime());

				System.out.println("- - Richiesta  inviata a "
						+ challengedPlayer.getUserData().getUsername());

				// avviso challenged player
				sendToPlayer(challengedPlayer.getUserData().getUsername(),
						result);

				System.out.println("- - SFIDANTE: "
						+ challengingPlayer.getUsername()); // LOG
				System.out.println("- - SFIDATO: "
						+ challengedPlayer.getUserData().getUsername()); // LOG
				newHumanOpponentFound = true;
				System.out.println("- - SFIDA CASUALE INVIATA: " + result[0]); // LOG
				this.addAutomaticRefuse(game, challengedPlayer.getUserData()
						.getUsername());
				break label;
			}
		}
		if (!newHumanOpponentFound) { // SFIDATO NON TROVATO
			System.out.println("NESSUN SFIDANTE TROVATO; MANDO IL BOT!!!");
			User user = botManager.allocateBot(gameId,
					challengingPlayer.getUsername(),
					challengingPlayer.getLevel());
			String[] msg = { "0210", String.valueOf(gameId),
					user.getUsername(), String.valueOf(user.getLevel()),
					String.valueOf(user.getImage()),
					String.valueOf(user.getFacebookPictureURL()),
					String.valueOf(user.getPlayedGames()),
					String.valueOf(user.getWonGames()),
					String.valueOf(user.getConsecutiveMatchWon()),
					String.valueOf(user.getHighestmatchScore()),
					String.valueOf(user.getHighestFirstRoundScore()),
					String.valueOf(user.getBestFirstRoundTime()),
					String.valueOf(user.getHighestSecondRoundScore()),
					String.valueOf(user.getBestSecondRoundTime()),
					String.valueOf(user.getHighestThirdRoundScore()),
					String.valueOf(user.getBestThirdRoundTime()), };

			System.out.println("Il Bot e: " + user.getUsername());

			int delay = 10 + random.nextInt(10);

			sendToPlayerWithDelay(challengingPlayer.getUsername(), msg, delay);

		}

	}

	/**
	 * 
	 * @param msgRcv
	 *            : msgRcv[0] comando , msgRcv[1] idGame
	 * @param channel
	 *            : canale del giocatore che ha rifiutato la sfida
	 * @return
	 */
	private void randomChallengeRefused(String[] msgRcv, Channel channel,
			boolean autoRefuse, String username) {

		Game game = jpae.gameLookUp(Long.valueOf(msgRcv[1]));

		System.out.println("Partita con ID:  " + Long.valueOf(msgRcv[1])
				+ "  rifiutata gia  " + game.getTimesHasBeenRefused()
				+ "  volte!!!");
		System.out.println("partita rifiutata da 1: "
				+ game.getFirstPlayerRejected());
		System.out.println("partita rifiutata da 2: "
				+ game.getSecondPlayerRejected());
		System.out.println("partita rifiutata da 3: "
				+ game.getThirdPlayerRejected());
		System.out.println("partita rifiutata da 4: "
				+ game.getFourthPlayerRejected());
		System.out.println("partita rifiutata da 5: "
				+ game.getFifthPlayerRejected());

		if (autoRefuse
				&& !game.hasBeenRejectedByPlayer(username)
				|| !autoRefuse
				&& channel != null
				&& !game.hasBeenRejectedByPlayer(playerMap.get(channel)
						.getUserData().getUsername())) {

			System.out.println("ENTRO NELLA PARTITA RIFIUTATA!!!!!");

			boolean newHumanOpponentFound = false;
			if (!autoRefuse) {
				addRejectingPlayer(Long.valueOf(msgRcv[1]),
						playerMap.get(channel).getUserData().getUsername());
				System.out
						.println("- - Un player ha rifiutato la sfida! Nome: "
								+ playerMap.get(channel).getUserData()
										.getUsername());
			} else {
				addRejectingPlayer(Long.valueOf(msgRcv[1]), username);
				System.out
						.println("- - AGGIUNTO AUTOMATICAMENTE  IL RIFIUTO! Nome: "
								+ username);
			}

			System.out.println("AGGIORNO LA PARTITA!!!!!!!!!Partita con ID:  "
					+ Long.valueOf(msgRcv[1]) + "  rifiutata  "
					+ game.getTimesHasBeenRefused() + "  volte!!!");
			System.out.println("partita rifiutata da 1: "
					+ game.getFirstPlayerRejected());
			System.out.println("partita rifiutata da 2: "
					+ game.getSecondPlayerRejected());
			System.out.println("partita rifiutata da 3: "
					+ game.getThirdPlayerRejected());
			System.out.println("partita rifiutata da 4: "
					+ game.getFourthPlayerRejected());
			System.out.println("partita rifiutata da 5: "
					+ game.getFifthPlayerRejected());

			// game.alreadyChallengedPlayers.add(channel); // aggiungo il
			// giocatore
			// che
			// ha rifiutato alla lista
			// dei giocatori non
			// interessati a giocare
			User challengingPlayer = jpae.humanUserLookUp(game.getPlayerOne());
			// challengingPlayer.addRejectingPlayer(playerMap.get(channel)
			// .getUserData().getUsername());

			System.out.println("partitia rifutata "
					+ game.getTimesHasBeenRefused() + " volte!");
			if (!(game.getTimesHasBeenRefused() > maxTimesGameCanBeRejected)) {
				label: for (Player challengedPlayer : playerList) {
					/*
					 * if (challengingPlayer.playerCh != p.playerCh &&
					 * p.getOpenGames() < 10 &&
					 * (challengingPlayer.getData().getLevel() >= (p.getData()
					 * .getLevel() - 5) && challengingPlayer.getData()
					 * .getLevel() <= (p.getData().getLevel() + 5)))
					 */
					/*
					 * && gameMap.get(idGame).alreadyChallengedPlayers
					 * .contains(p.playerCh)) Verifica se p e gia stato sfidato
					 * (ma solo in questa iterazione)
					 */

					if (challengingPlayer.getUsername() != challengedPlayer
							.getUserData().getUsername()
							&& challengedPlayer.getUserData().getLevel() >= challengingPlayer
									.getLevel() - matchMakingRange / 2
							&& challengedPlayer.getUserData().getLevel() <= challengingPlayer
									.getLevel() + matchMakingRange / 2
							&& !jpae.gameAlreadyExists(challengingPlayer
									.getUsername(), challengedPlayer
									.getUserData().getUsername())
							&& !game.hasBeenRejectedByPlayer(challengedPlayer
									.getUserData().getUsername())) {
						String[] result = new String[16];
						result[0] = "0206";
						result[1] = msgRcv[1];// gameId
						result[2] = String.valueOf(challengingPlayer
								.getUsername());
						result[3] = String
								.valueOf(challengingPlayer.getLevel());
						result[4] = String
								.valueOf(challengingPlayer.getImage());
						result[5] = String.valueOf(challengingPlayer
								.getFacebookPictureURL());
						result[6] = String.valueOf(challengingPlayer
								.getPlayedGames());
						result[7] = String.valueOf(challengingPlayer
								.getWonGames());
						result[8] = String.valueOf(challengingPlayer
								.getConsecutiveMatchWon());
						result[9] = String.valueOf(challengingPlayer
								.getHighestmatchScore());
						result[10] = String.valueOf(challengingPlayer
								.getHighestFirstRoundScore());
						result[11] = String.valueOf(challengingPlayer
								.getBestFirstRoundTime());
						result[12] = String.valueOf(challengingPlayer
								.getHighestSecondRoundScore());
						result[13] = String.valueOf(challengingPlayer
								.getBestSecondRoundTime());
						result[14] = String.valueOf(challengingPlayer
								.getHighestThirdRoundScore());
						result[15] = String.valueOf(challengingPlayer
								.getBestThirdRoundTime());

						// avviso challenged player
						sendToPlayer(challengedPlayer.getUserData()
								.getUsername(), result);

						/*
						 * System.out.println("- - Richiesta  inviata a " +
						 * challengedPlayer.getUserData().getUsername());
						 * 
						 * System.out.println("- - SFIDANTE: " +
						 * challengingPlayer.getUsername()); // LOG
						 * System.out.println("- -SFIDATO: " +
						 * challengedPlayer.getUserData().getUsername()); // LOG
						 */
						newHumanOpponentFound = true;
						this.addAutomaticRefuse(game, challengedPlayer
								.getUserData().getUsername());
						break label;
					}
				}
			}
			if (!newHumanOpponentFound) { // SFIDATO NON TROVATO
				System.out.println("NESSUN SFIDANTE TROVATO; MANDO IL BOT!!!");
				User user = botManager.allocateBot(Long.valueOf(msgRcv[1]),
						challengingPlayer.getUsername(),
						challengingPlayer.getLevel());
				String[] msg = { "0210", String.valueOf(msgRcv[1]),
						user.getUsername(), String.valueOf(user.getLevel()),
						String.valueOf(user.getImage()),
						String.valueOf(user.getFacebookPictureURL()),
						String.valueOf(user.getPlayedGames()),
						String.valueOf(user.getWonGames()),
						String.valueOf(user.getConsecutiveMatchWon()),
						String.valueOf(user.getHighestmatchScore()),
						String.valueOf(user.getHighestFirstRoundScore()),
						String.valueOf(user.getBestFirstRoundTime()),
						String.valueOf(user.getHighestSecondRoundScore()),
						String.valueOf(user.getBestSecondRoundTime()),
						String.valueOf(user.getHighestThirdRoundScore()),
						String.valueOf(user.getBestThirdRoundTime()), };

				System.out.println("Il Bot  e: " + user.getUsername());

				int delay;
				if (game.getTimesHasBeenRefused() > 2)
					delay = 0;
				else if (game.getTimesHasBeenRefused() == 2)
					delay = random.nextInt(10);
				else
					delay = 10 + random.nextInt(10);
				if (autoRefuse)
					delay = 1;

				sendToPlayerWithDelay(challengingPlayer.getUsername(), msg,
						delay);

			}
		} else
			System.out
					.println("RIFIUTO GIA ARRIVATO! EVITO DI CICLARE DI NUOVO!!!!");

	}

	private void randomChallengeAccepted(String[] msgRcv, Channel channel) {

		Long idGame = Long.valueOf(msgRcv[1]);
		Player challengedPlayer = playerMap.get(channel);
		User user = challengedPlayer.getUserData();
		String[] msg = { "0210", msgRcv[1],
				challengedPlayer.getUserData().getUsername(),
				String.valueOf(user.getLevel()),
				String.valueOf(user.getImage()),
				String.valueOf(user.getFacebookPictureURL()),
				String.valueOf(user.getPlayedGames()),
				String.valueOf(user.getWonGames()),
				String.valueOf(user.getConsecutiveMatchWon()),
				String.valueOf(user.getHighestmatchScore()),
				String.valueOf(user.getHighestFirstRoundScore()),
				String.valueOf(user.getBestFirstRoundTime()),
				String.valueOf(user.getHighestSecondRoundScore()),
				String.valueOf(user.getBestSecondRoundTime()),
				String.valueOf(user.getHighestThirdRoundScore()),
				String.valueOf(user.getBestThirdRoundTime()), };

		System.out.println("- - Lobby Random Challenge Accepted. ID Game:"
				+ idGame);
		setPlayerTwoIntoGame(idGame, channel);
		incrementOpenGames(playerMap.get(channel));

		String challengingPlayer = this.returnOpponentName(idGame,
				challengedPlayer.getUserData().getUsername());

		sendToPlayer(challengingPlayer, msg);

	}

	private void roundEnd(String[] msgRcv, Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.gameLookUp(Long.valueOf(msgRcv[1]));

		System.out.println("- - Lobby Round End");
		System.out.println("- - GAMEID: " + msgRcv[1] + " EXP: " + msgRcv[2]);

		String playerWhoFinishedRound = playerMap.get(channel).getUserData()
				.getUsername();
		String otherPlayer = returnOpponentName(game.getId(),
				playerWhoFinishedRound);

		String[] output = { "1006", String.valueOf(msgRcv[1]), msgRcv[2], "" };

		if (game.getGameState().equals("WaitingRoundOne")) {
			if (playerWhoFinishedRound.equals(game.getPlayerTwo())) {
				game.setPlayerTwoScoreOne(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerTwo(), Integer.valueOf(msgRcv[2]));
				updateFirstRoundUserRecords(game.getPlayerTwo(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player Two Score One: "
						+ game.getPlayerTwoScoreOne()); // LOG
			} else {
				game.setPlayerOneScoreOne(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerOne(), Integer.valueOf(msgRcv[2]));
				updateFirstRoundUserRecords(game.getPlayerOne(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player One Score One: "
						+ game.getPlayerOneScoreOne()); // LOG
				if (this.checkIfPlayerIsBot(otherPlayer)) {
					System.out.println("GENERO A CASO IL PUNTEGGIO!!!!");
					game.setPlayerTwoScoreOne(botManager
							.generateFirstRoundRandomScore(jpae
									.humanUserLookUp(playerWhoFinishedRound)));
					updateFirstRoundUserRecords(
							game.getPlayerTwo(),
							game.getPlayerTwoScoreOne(),
							Math.round((5f + random.nextFloat() * 5f) * 10.0f) / 10.0f);
					System.out.println("SCORE DEL BOT : "
							+ game.getPlayerTwoScoreOne()); // LOG
					updateUserExp(game.getPlayerTwo(),
							game.getPlayerTwoScoreOne());
					String[] messageToHumanPlayer = { "1006",
							String.valueOf(msgRcv[1]),
							String.valueOf(game.getPlayerTwoScoreOne()), "" };
					int delay = random.nextInt(20);
					sendToPlayerWithDelay(playerWhoFinishedRound,
							messageToHumanPlayer, delay);

				}
			}
		} else if (game.getGameState().equals("WaitingRoundTwo")) {
			if (playerWhoFinishedRound.equals(game.getPlayerTwo())) {
				game.setPlayerTwoScoreTwo(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerTwo(), Integer.valueOf(msgRcv[2]));
				updateSecondRoundUserRecords(game.getPlayerTwo(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player Two Score Two: "
						+ game.getPlayerTwoScoreTwo()); // LOG
			} else {
				game.setPlayerOneScoreTwo(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerOne(), Integer.valueOf(msgRcv[2]));
				updateSecondRoundUserRecords(game.getPlayerOne(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player One Score Two: "
						+ game.getPlayerOneScoreTwo()); // LOG
				if (this.checkIfPlayerIsBot(otherPlayer)) {
					System.out.println("GENERO A CASO IL PUNTEGGIO!!!!");
					game.setPlayerTwoScoreTwo(botManager
							.generateSecondRoundRandomScore(jpae
									.humanUserLookUp(playerWhoFinishedRound)));
					updateSecondRoundUserRecords(
							game.getPlayerTwo(),
							game.getPlayerTwoScoreTwo(),
							Math.round((5f + random.nextFloat() * 5f) * 10.0f) / 10.0f);
					System.out.println("SCORE DEL BOT : "
							+ game.getPlayerTwoScoreTwo()); // LOG
					updateUserExp(game.getPlayerTwo(),
							game.getPlayerTwoScoreTwo());
					String[] messageToHumanPlayer = { "1006",
							String.valueOf(msgRcv[1]),
							String.valueOf(game.getPlayerTwoScoreTwo()), "" };
					int delay = random.nextInt(20);
					sendToPlayerWithDelay(playerWhoFinishedRound,
							messageToHumanPlayer, delay);

				}
			}
		} else if (game.getGameState().equals("WaitingRoundThree")) {
			if (playerWhoFinishedRound.equals(game.getPlayerTwo())) {
				game.setPlayerTwoScoreThree(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerTwo(), Integer.valueOf(msgRcv[2]));
				updateThirdRoundUserRecords(game.getPlayerTwo(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player Two Score Three: "
						+ game.getPlayerTwoScoreThree()); // LOG
			} else {
				game.setPlayerOneScoreThree(Integer.valueOf(msgRcv[2]));
				updateUserExp(game.getPlayerOne(), Integer.valueOf(msgRcv[2]));
				updateThirdRoundUserRecords(game.getPlayerOne(),
						Integer.valueOf(msgRcv[2]), Float.valueOf(msgRcv[3]));
				System.out.println("Player One Score Three: "
						+ game.getPlayerOneScoreThree()); // LOG
				if (this.checkIfPlayerIsBot(otherPlayer)) {
					System.out.println("GENERO A CASO IL PUNTEGGIO!!!!");
					game.setPlayerTwoScoreThree(botManager
							.generateThirdRoundRandomScore(jpae
									.humanUserLookUp(playerWhoFinishedRound)));
					updateThirdRoundUserRecords(game.getPlayerTwo(),
							game.getPlayerTwoScoreThree(), 0);
					System.out.println("SCORE DEL BOT : "
							+ game.getPlayerTwoScoreThree()); // LOG
					updateUserExp(game.getPlayerTwo(),
							game.getPlayerTwoScoreThree());
					String[] messageToHumanPlayer = { "1006",
							String.valueOf(msgRcv[1]),
							String.valueOf(game.getPlayerTwoScoreThree()), "" };
					int delay = random.nextInt(20);
					sendToPlayerWithDelay(playerWhoFinishedRound,
							messageToHumanPlayer, delay);

				}
			}
		}

		sendToPlayer(otherPlayer, output);
		this.updateGameStatus(game);

		if (game.getGameState().equals("GameEnded") && !game.isClosed()) {
			matchEnd(game);
		} else {
			if (commitRequested) {
				jpae.getEm().getTransaction().commit();
			}

		}

		System.out.println("- - Lobby, RIsultato Round END: " + output[0]);
		System.out.println("- - Lobby, RIsultato Round END: " + output[1]);
		System.out.println("- - Lobby, RIsultato Round END: " + output[2]);

	}

	public void matchEnd(Game game) {
		int PlayerOneCoins = 2;
		int PlayerOneRoundsWon = 0;
		int PlayerTwoCoins = 2;
		int PlayerTwoRoundsWon = 0;

		if (game.getPlayerOneScoreOne() > game.getPlayerTwoScoreOne()) {
			PlayerOneCoins += 2;
			PlayerOneRoundsWon++;
		} else if ((game.getPlayerOneScoreOne() == game.getPlayerTwoScoreOne())
				&& game.getPlayerOneScoreOne() != 0) {
			PlayerOneCoins += 2;
			PlayerTwoCoins += 2;
			PlayerOneRoundsWon++;
			PlayerTwoRoundsWon++;
		} else {
			PlayerTwoCoins += 2;
			PlayerTwoRoundsWon++;
		}

		if (game.getPlayerOneScoreTwo() > game.getPlayerTwoScoreTwo()) {
			PlayerOneCoins += 2;
			PlayerOneRoundsWon++;
		} else if ((game.getPlayerOneScoreTwo() == game.getPlayerTwoScoreTwo())
				&& game.getPlayerOneScoreTwo() != 0) {
			PlayerOneCoins += 2;
			PlayerTwoCoins += 2;
			PlayerOneRoundsWon++;
			PlayerTwoRoundsWon++;
		} else {
			PlayerTwoCoins += 2;
			PlayerTwoRoundsWon++;
		}

		if (game.getPlayerOneScoreThree() > game.getPlayerTwoScoreThree()) {
			PlayerOneCoins += 2;
			PlayerOneRoundsWon++;
		} else if ((game.getPlayerOneScoreThree() == game
				.getPlayerTwoScoreThree())
				&& game.getPlayerOneScoreThree() != 0) {
			PlayerOneCoins += 2;
			PlayerTwoCoins += 2;
			PlayerOneRoundsWon++;
			PlayerTwoRoundsWon++;
		} else {
			PlayerTwoCoins += 2;
			PlayerTwoRoundsWon++;
		}

		if (PlayerOneRoundsWon > PlayerTwoRoundsWon)
			PlayerOneCoins += 4;
		else if (PlayerOneRoundsWon == PlayerTwoRoundsWon) {
			PlayerOneCoins += 4;
			PlayerTwoCoins += 4;
		} else {
			PlayerTwoCoins += 4;
		}

		String[] msgPlayerOne = { "1008", String.valueOf(game.getId()),
				String.valueOf(PlayerOneCoins) };
		System.out.println("gameID: " + game.getId() + " PILLS: "
				+ PlayerOneCoins); // LOG
		sendToPlayer(game.getPlayerOne(), msgPlayerOne);
		updateUserPills(game.getPlayerOne(), PlayerOneCoins);
		decrementOpenGames(game.getPlayerOne());
		incrementPlayedGames(game.getPlayerOne());

		String[] msgPlayerTwo = { "1008", String.valueOf(game.getId()),
				String.valueOf(PlayerTwoCoins) };
		System.out.println("gameID: " + game.getId() + " PILLS: "
				+ PlayerTwoCoins); // LOG
		sendToPlayer(game.getPlayerTwo(), msgPlayerTwo);
		updateUserPills(game.getPlayerTwo(), PlayerTwoCoins);
		decrementOpenGames(game.getPlayerTwo());
		incrementPlayedGames(game.getPlayerTwo());

		if (PlayerOneCoins > PlayerTwoCoins) {
			this.incrementWonGames(game.getPlayerOne());
			this.updateMatchesUserRecords(game.getPlayerOne(),
					game.getPlayerOneScoreOne() + game.getPlayerOneScoreTwo()
							+ game.getPlayerOneScoreThree(), true);
			this.updateMatchesUserRecords(game.getPlayerTwo(),
					game.getPlayerTwoScoreOne() + game.getPlayerTwoScoreTwo()
							+ game.getPlayerTwoScoreThree(), false);
		} else {
			this.incrementWonGames(game.getPlayerTwo());
			this.updateMatchesUserRecords(game.getPlayerOne(),
					game.getPlayerOneScoreOne() + game.getPlayerOneScoreTwo()
							+ game.getPlayerOneScoreThree(), false);
			this.updateMatchesUserRecords(game.getPlayerTwo(),
					game.getPlayerTwoScoreOne() + game.getPlayerTwoScoreTwo()
							+ game.getPlayerTwoScoreThree(), true);
		}

		game.setClosed(true);

		if (jpae.getEm().getTransaction().isActive()) {
			System.out.println("Match CONCLUSO!!! SALVO NEL DB!!!");
			jpae.getEm().getTransaction().commit();
		}

	}

	private void pillsAndXpBonusReceived(String[] msg, Channel channel) {

		Long gameId = Long.valueOf(msg[1]);
		String username = playerMap.get(channel).getUserData().getUsername();
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.gameLookUp(gameId);
		if (game.getPlayerOne().equals(username))
			game.setPlayerOneReceivedPills(true);
		else
			game.setPlayerTwoReceivedPills(true);

		User user = jpae.generalUserLookUp(username);
		int exp = Integer.valueOf(msg[2]);

		if (levelManager.checkLevelIncreasing(user, exp) != 0) {
			exp = levelManager.checkLevelIncreasing(user, exp);
			user.setLevel(user.getLevel() + 1);
			user.setExp(0);
			System.out.println("Il player " + username
					+ " e aumentato di livello!!! Aggiungo solo " + exp
					+ " punti esperienza");
		}
		System.out.println("Aggiorno gli EXP di " + username + ". "
				+ user.getExp() + " ---> " + (user.getExp() + exp));
		user.setExp(user.getExp() + exp);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	private void challengeFriend(String friendName, Channel channel) {

		if (!jpae.gameAlreadyExists(playerMap.get(channel).getUserData()
				.getUsername(), friendName)) {
			System.out.println("- - Lobby Challenge a Friend");
			Player challengingPlayer = playerMap.get(channel);
			Game game = new Game(challengingPlayer.getUserData().getUsername());
			game.setTimesHasBeenRefused(0);
			game.setClosed(false);

			// ///PERSISTENZA
			jpae.gameCreate(game);
			// ///Persistenza

			if (this.checkIfPlayerIsBot(friendName)) {

				User challengedPlayer = jpae.botLookUp(friendName);
				Long gameID = game.getId();
				String[] msg = {
						"0210",
						String.valueOf(gameID),
						challengedPlayer.getUsername(),
						String.valueOf(challengedPlayer.getLevel()),
						String.valueOf(challengedPlayer.getImage()),
						String.valueOf(challengedPlayer.getFacebookPictureURL()),
						String.valueOf(challengedPlayer.getPlayedGames()),
						String.valueOf(challengedPlayer.getWonGames()),
						String.valueOf(challengedPlayer
								.getConsecutiveMatchWon()),
						String.valueOf(challengedPlayer.getHighestmatchScore()),
						String.valueOf(challengedPlayer
								.getHighestFirstRoundScore()),
						String.valueOf(challengedPlayer.getBestFirstRoundTime()),
						String.valueOf(challengedPlayer
								.getHighestSecondRoundScore()),
						String.valueOf(challengedPlayer
								.getBestSecondRoundTime()),
						String.valueOf(challengedPlayer
								.getHighestThirdRoundScore()),
						String.valueOf(challengedPlayer.getBestThirdRoundTime()), };

				System.out.println("- - SFIDATO BOOT; CREO IN AUTOMATICO!!!"
						+ gameID);
				setPlayerTwoIntoGame(gameID, challengedPlayer.getUsername());
				incrementOpenGames(challengedPlayer.getUsername());
				incrementOpenGames(challengedPlayer.getUsername());

				int delay = 25+random.nextInt(35);

				this.sendToPlayerWithDelay(challengingPlayer.getUserData()
						.getUsername(), msg, 1);

			} else {
				User user = challengingPlayer.getUserData();
				String[] msg = { "0306", String.valueOf(game.getId()),
						challengingPlayer.getUserData().getUsername(),
						String.valueOf(user.getLevel()),
						String.valueOf(user.getImage()),
						String.valueOf(user.getFacebookPictureURL()),
						String.valueOf(user.getPlayedGames()),
						String.valueOf(user.getWonGames()),
						String.valueOf(user.getConsecutiveMatchWon()),
						String.valueOf(user.getHighestmatchScore()),
						String.valueOf(user.getHighestFirstRoundScore()),
						String.valueOf(user.getBestFirstRoundTime()),
						String.valueOf(user.getHighestSecondRoundScore()),
						String.valueOf(user.getBestSecondRoundTime()),
						String.valueOf(user.getHighestThirdRoundScore()),
						String.valueOf(user.getBestThirdRoundTime()), };

				this.sendToPlayer(friendName, msg);
			}
		}

	}

	private void friendChallengeRefused(String friendName, Channel channel) {

		System.out.println("- - Lobby Refuse a Friend Challenge");
		Player refusingPlayer = playerMap.get(channel);

		String[] msg = { "0304", refusingPlayer.getUserData().getUsername() };

		this.sendToPlayer(friendName, msg);

	}

	private void checkPlayerAndInfo(String playerToSearchFor, Channel channel) {
		String[] result = { null, null, null };
		User user = jpae.generalUserLookUp(playerToSearchFor);
		if (user != null) {

			String[] msg = { "0901", user.getUsername(),
					String.valueOf(user.getLevel()),
					String.valueOf(user.getImage()),
					String.valueOf(user.getFacebookPictureURL()),
					String.valueOf(user.getPlayedGames()),
					String.valueOf(user.getWonGames()),
					String.valueOf(user.getConsecutiveMatchWon()),
					String.valueOf(user.getHighestmatchScore()),
					String.valueOf(user.getHighestFirstRoundScore()),
					String.valueOf(user.getBestFirstRoundTime()),
					String.valueOf(user.getHighestSecondRoundScore()),
					String.valueOf(user.getBestSecondRoundTime()),
					String.valueOf(user.getHighestThirdRoundScore()),
					String.valueOf(user.getBestThirdRoundTime()), };
			this.sendToPlayer(playerMap.get(channel).getUserData()
					.getUsername(), msg);
		} else {
			result[0] = "0902";
			this.sendToPlayer(playerMap.get(channel).getUserData()
					.getUsername(), result);
		}

	}

	private void recoverPassword(String[] msg, Channel channel) {

		System.out.println("RECUPERO PASSWORD di: " + msg[1]);
		User user = jpae.userEmailBasedLookUp(msg[1]);
		String username = user.getUsername();
		String password = user.getPassword();
		System.out.println("PASSWORD: " + password);
		EmailSender.sendEmail(msg[1], username, password);
		System.out.println("EMAIL INVIATA");
	}

	/**
	 * 
	 * @param Channel
	 *            ch
	 * @param cmd
	 * @param msg
	 */
	public boolean sendToPlayer(final String username, final String[] msg) {

		System.out.println("- - Invio Messaggio: ");
		if (this.isPlayerLoggedIn(username)) {
			// Player ONLINE
			// Player player = getPlayerFromUserName(username);
			Channel channel = getPlayerFromUserName(username).getChannel();
			Message tempMessage = new Message(username, msg);
			messageMap.put(messageIDCounter, tempMessage);
			// player.addMessageWaitingForAck(messageID);
			System.out.println("Messaggio aggiunto! dimensioni: "
					+ messageMap.size());
			String[] message = new String[msg.length + 1];
			message[0] = String.valueOf(messageIDCounter);
			for (int i = 1; i < message.length; i++)
				message[i] = msg[i - 1];
			channel.writeAndFlush(jFields.toJson(message) + "\r\n");
			this.addResendingMessageTask(username, messageIDCounter);
			// ///////////////////////////////SCHEDULE TASK

			/*
			 * ScheduledFuture<?> future; future =
			 * channel.eventLoop().schedule(new Runnable() {
			 * 
			 * @Override public void run() { sendToPlayer(username, msg); } },
			 * 35, TimeUnit.SECONDS);
			 * 
			 * future.cancel(false);
			 */
			// ///////////////////////////////SCHEDULE TASK

			messageIDCounter++;
			System.out.println("- - Messaggio inviato a " + username
					+ ". Contenuto: ");
			System.out.println("MSG_ID: " + message[0] + "  ");
			System.out.println("CMD: " + message[1] + "  ");
			for (int i = 2; i < message.length; i++)
				System.out.println(message[i] + "  ");
			return true;
		} else if (this.checkIfPlayerIsBot(username)) {
			// Player BOT
			System.out.println("- - Il player e un BOT! Nessun invio "
					+ username);

			return false;
		} else {
			// Player OFFLINE
			System.out
					.println("- - Il player e offline! Aggiungo il messaggio allo stack messaggi!");
			if (msg[0].equals("0210")) {
				System.out
						.println("Messaggio per nuova partita, evito di salvare perchè se ne occuperà la sincronizzazione!");
			} else {
				System.out.println("- - Salvo messaggio per" + username);
				for (int j = 0; j < msg.length; j++)
					System.out.println("- - " + msg[j]);
				jpae.messageCreate(new Message(username, msg));
			}

			return false;

		}

	}

	public boolean sendToPlayerWithDelay(final String username,
			final String[] msg, final int delay) {

		System.out.println("- - Invio Messaggio: ");
		if (this.isPlayerLoggedIn(username)) {
			// Player ONLINE
			Channel ch = this.getPlayerFromUserName(username).getChannel();
			// ///////////////////////////////SCHEDULE TASK
			ScheduledFuture<?> future = ch.eventLoop().schedule(new Runnable() {
				@Override
				public void run() {
					System.out.println("ESEGUO!!!");
					sendToPlayer(username, msg);
				}
			}, delay, TimeUnit.SECONDS);
			// ///////////////////////////////SCHEDULE TASK
			return true;
		} else if (this.checkIfPlayerIsBot(username)) {
			// Player BOT
			System.out.println("- - Il player e un BOT! Nessun invio "
					+ username);

			return false;
		} else {
			// Player OFFLINE
			System.out
					.println("- - Il player e offline! Aggiungo il messaggio allo stack messaggi!");
			System.out.println("- - Salvo messaggio per" + username);
			for (int j = 0; j < msg.length; j++)
				System.out.println("- - " + msg[j]);
			jpae.messageCreate(new Message(username, msg));

			return false;

		}
	}

	public boolean sendToPlayer(final Channel channel, String[] msg) {

		System.out.println("- - Invio Messaggio DIRETTO tramite channel: ");
		if (channel != null) {
			Message tempMessage = new Message(null, msg);
			messageMap.put(messageIDCounter, tempMessage);
			System.out.println("Messaggio aggiunto! dimensioni: "
					+ messageMap.size());
			String[] message = new String[msg.length + 1];
			message[0] = String.valueOf(messageIDCounter);
			for (int i = 1; i < message.length; i++)
				message[i] = msg[i - 1];
			channel.writeAndFlush(jFields.toJson(message) + "\r\n");
			messageIDCounter++;
			System.out.println("- - Messaggio inviato " + ". Contenuto: ");
			System.out.println("MSG_ID: " + message[0] + "  ");
			System.out.println("CMD: " + message[1] + "  ");
			for (int i = 2; i < message.length; i++)
				System.out.println(message[i] + "  ");
			return true;
		}

		return false;

	}

	public String returnOpponentName(Long idGame, String playerName) {

		Game game = jpae.gameLookUp(idGame);

		if (game.getPlayerOne().equals(playerName))
			return game.getPlayerTwo();
		else
			return game.getPlayerOne();

	}

	public Player getPlayerFromUserName(String userName) {

		for (Player p : playerList)
			if (p.getUserData().getUsername().equals(userName))
				return p;
		return null;

	}

	public void setPlayerTwoIntoGame(Long idGame, Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.getEm().find(Game.class, idGame);
		game.setPlayerTwo(playerMap.get(channel).getUserData().getUsername());
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}
	}

	public void setPlayerTwoIntoGame(Long idGame, String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.getEm().find(Game.class, idGame);
		game.setPlayerTwo(username);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}
	}

	public boolean isPlayerLoggedIn(String playerName) {

		System.out.print("- - Verifico se " + playerName + " e online...");
		for (Player p : playerList) {
			if (p.getUserData().getUsername().equals(playerName)) {
				System.out.println("ONLINE!");
				return true;
			}
		}

		System.out.println("OFFLINE!!!");
		return false;
	}

	public boolean isPlayerLoggedIn(Channel channel) {

		if (channel != null) {
			Player p = playerMap.get(channel);
			if (p != null) {
				return true;
			}
		}
		return false;
	}

	public boolean checkIfPlayerIsBot(String username) {

		if (jpae.botLookUp(username) == null)
			return false;
		return true;
	}

	public void emptyMessageStack(Channel channel) {

		String playerName = playerMap.get(channel).getUserData().getUsername();

		List<Message> msgList = jpae.getMessageList(playerName);

		if (msgList.size() > 0) {
			System.out.println("Il player " + playerName + " ha "
					+ msgList.size() + " messaggi in sospeso!!!");

			for (int i = 0; i < msgList.size(); i++) {
				Message msg = msgList.get(i);
				String[] message = msg.getMessage();
				System.out.println("Messaggio per: " + msg.getPlayerName()
						+ ". Id del Messaggio: " + msg.getId()
						+ ". DIMENSIONI DEL MESSAGGIO:" + message.length);
				for (int j = 0; j < message.length; j++)
					System.out.println(message[j]);
				this.sendToPlayer(playerName, message);
				// msgList.remove(msg);
				jpae.messageDelete(msg);
			}
		} else
			System.out.println("Il player " + playerName
					+ " NON ha messaggi in sospeso!!!");

	}

	public void updateGameStatus(Game game) {

		if (game.getPlayerOneScoreThree() != -1
				&& game.getPlayerTwoScoreThree() != -1) {
			game.setGameState("GameEnded");
		} else if (game.getPlayerOneScoreTwo() != -1
				&& game.getPlayerTwoScoreTwo() != -1) {
			game.setGameState("WaitingRoundThree");
		} else if (game.getPlayerOneScoreOne() != -1
				&& game.getPlayerTwoScoreOne() != -1) {
			game.setGameState("WaitingRoundTwo");
		}
		System.out.println("NEW_GAME_STATE: " + game.getGameState());// LOG
	}

	public void changeEmail(Channel channel, String email) {

		System.out.println("Cambio l'indirizzo email con: " + email);
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setEmail(email);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}
	}

	public void changePassword(Channel channel, String password) {
		System.out.println("Cambio la password con: " + password);
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPassword(password);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void changeImageId(Channel channel, String imageId) {
		System.out.println("Cambio l'imageId con: " + imageId);
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setImage(Integer.valueOf(imageId));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void setFacebookInfo(Channel channel, String facebookID,
			String facebookPictureURL) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		System.out
				.println("Schermata di richiesta settaggio FACEBOOK Info per l'utente: "
						+ user.getUsername());
		System.out.println("FacebookID:  " + facebookID);

		if (jpae.userLookUpFacebookId(facebookID) == null) {
			System.out
					.println("l'ID Facebook non è associato a nessun altro account, ora lo sono!");
			user.setFacebookId(facebookID);
			user.setFacebookPictureURL(facebookPictureURL);
		} else {
			System.out
					.println("L'account  è già bindato a  un account facebook!!!!");
		}
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void messageTest() {
		System.out
				.println("////////////////////////////////MESSAGE TEST////////////////////////");
		String name = "Daniele";
		String[] message = { "999", "ciao", "Daniele" };

		Message msg = new Message(name, message);
		if (jpae.messageCreate(msg).equals("TRUE"))
			System.out.println("Messaggio salvato con successo!!");

		if (jpae.getMessageList(name) != null) {
			System.out.println("Messaggio caricato con successo!!");
			List<Message> msgList = jpae.getMessageList(name);
			for (int i = 0; i < msgList.size(); i++) {
				Message msgTemp = msgList.get(i);
				String[] text = msgTemp.getMessage();
				System.out.println("Messaggio per: " + msgTemp.getPlayerName());
				for (int j = 0; j < text.length; j++)
					System.out.println(text[j]);
			}

		}

		System.out
				.println("////////////////////////////////MESSAGE TEST////////////////////////");
	}

	public void updateUserExp(String userName, int exp) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		User user = jpae.generalUserLookUp(userName);
		if (levelManager.checkLevelIncreasing(user, exp) != 0) {
			exp = levelManager.checkLevelIncreasing(user, exp);
			user.setLevel(user.getLevel() + 1);
			user.setExp(0);
			System.out.println("Il player " + userName
					+ " e aumentato di livello!!! Aggiungo solo " + exp
					+ " punti esperienza");
		}
		System.out.println("Aggiorno gli EXP di " + userName + ". "
				+ user.getExp() + " ---> " + (user.getExp() + exp));
		user.setExp(user.getExp() + exp);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void updateUserPills(String userName, int pills) {

		User user = jpae.generalUserLookUp(userName);
		System.out.println("Aggiorno le PILLOLE di " + userName + ". "
				+ user.getPills() + " ---> " + (user.getPills() + pills));
		user.setPills(user.getPills() + pills);

	}

	public static int getMatchmakingRange() {
		return matchMakingRange;
	}

	public void incrementOpenGames(Player player) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(player.getUserData().getUsername());
		user.setOpenGames(user.getOpenGames() + 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void incrementOpenGames(String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(username);
		user.setOpenGames(user.getOpenGames() + 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void incrementPowerUpTime(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Time(user.getPowerUp_Time() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);

	}

	public void incrementPowerUpDoubleCouple(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_DoubleCouple(user.getPowerUp_DoubleCouple() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);

	}

	public void incrementPowerUpFaceUp(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_FaceUp(user.getPowerUp_FaceUp() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);
	}

	public void incrementPowerUpStop(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Stop(user.getPowerUp_Stop() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);

	}

	public void incrementPowerUpChain(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Chain(user.getPowerUp_Chain() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);

	}

	public void incrementPowerUpBonus(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Bonus(user.getPowerUp_Bonus() + 1);
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		String[] msgToSend = { "1003", msg[1] };
		this.sendToPlayer(playerMap.get(channel).getUserData().getUsername(), msgToSend);

	}

	public void decrementPowerUpTime(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Time(user.getPowerUp_Time() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPowerUpDoubleCouple(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_DoubleCouple(user.getPowerUp_DoubleCouple() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPowerUpFaceUp(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_FaceUp(user.getPowerUp_FaceUp() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPowerUpStop(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Stop(user.getPowerUp_Stop() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPowerUpChain(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Chain(user.getPowerUp_Chain() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPowerUpBonus(Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPowerUp_Bonus(user.getPowerUp_Bonus() - 1);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementPills(Channel channel, String[] msg) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(playerMap.get(channel).getUserData()
				.getUsername());
		user.setPills(user.getPills() - Integer.valueOf(msg[1]));
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void decrementOpenGames(String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(username);
		user.setOpenGames(user.getOpenGames() - 1);
		System.out
				.println("Decremento il numero di partite ATTIVE! ora uguali a "
						+ user.getOpenGames() + " per " + user.getUsername());
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}
	}

	public void incrementPlayedGames(String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(username);
		user.setPlayedGames(user.getPlayedGames() + 1);
		System.out
				.println("Incremento il numero di partite GIOCATE(CONCLUSE)! ora uguali a "
						+ user.getPlayedGames() + " per " + user.getUsername());
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void manageGivenUpGames(Game game, String playerWhoGaveUp,
			String playerForcedToCloseMatch) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		this.decrementOpenGames(playerWhoGaveUp);
		this.incrementPlayedGames(playerWhoGaveUp);
		this.incrementGivenUpGames(playerWhoGaveUp);

		this.decrementOpenGames(playerForcedToCloseMatch);
		this.incrementPlayedGames(playerForcedToCloseMatch);
		this.incrementWonGames(playerForcedToCloseMatch);

		this.closeMatch(game, playerWhoGaveUp);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void incrementGivenUpGames(String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(username);
		user.setGivenUpMatches(user.getGivenUpMatches() + 1);
		System.out
				.println("Incremento il numero di partite ABBANDONATE! ora uguali a "
						+ user.getGivenUpMatches()
						+ " per "
						+ user.getUsername());
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void incrementWonGames(String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		User user = jpae.generalUserLookUp(username);
		user.setWonGames(user.getWonGames() + 1);
		System.out
				.println("Incremento il numero di partite VINTE! ora uguali a "
						+ user.getWonGames() + " per " + user.getUsername());
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void addRejectingPlayer(Long gameId, String username) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.gameLookUp(gameId);
		game.addRejectingPlayer(username);
		game.setTimesHasBeenRefused(game.getTimesHasBeenRefused() + 1);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void closeMatch(Game game, String playerWhoGaveUp) {
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		game.setClosed(true);
		game.setClosedForGiveUp(true);
		game.setPlayerWhoGaveUp(playerWhoGaveUp);
		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void addResendingMessageTask(final String username, final Long msgID) {

		if (this.isPlayerLoggedIn(username)) {
			this.getPlayerFromUserName(username).addMessageWaitingForAck(msgID,
					scheduler.newTimeout(new TimerTask() {
						@Override
						public void run(Timeout arg0) throws Exception {
							reSendToPlayer(username, msgID);
						}
					}, timeToResendMessage, TimeUnit.SECONDS));
		}
	}

	public void addAutomaticRefuse(final Game game, final String username) {

		final String[] gameID = { "", String.valueOf(game.getId()) };
		scheduler.newTimeout(new TimerTask() {
			@Override
			public void run(Timeout arg0) throws Exception {
				if (!game.hasBeenRejectedByPlayer(username)
						&& game.getPlayerTwo().equals("")) {
					randomChallengeRefused(gameID, null, true, username);
					System.out
							.println("IL RIFIUTO NON E ARRIVATO, LO FACCIO PARTIRE AUTOAMTICAMENTE!!!!");
				} else
					System.out
							.println("IL RIFIUTO E ARRIVATO!!!! TUTT OK! SENZA PENSIERI!!!!");
			}
		}, timeToAutomaticRefuse, TimeUnit.SECONDS);

	}

	public boolean reSendToPlayer(final String username, final Long msgID) {

		System.out.println("- - Invio Messaggio: ");
		Message MessageWrapper = messageMap.get(msgID);
		String[] msg = MessageWrapper.getMessage();
		if (this.isPlayerLoggedIn(username)) {
			Channel channel = getPlayerFromUserName(username).getChannel();
			String[] message = new String[msg.length + 1];
			message[0] = String.valueOf(msgID);
			for (int i = 1; i < message.length; i++)
				message[i] = msg[i - 1];
			channel.writeAndFlush(jFields.toJson(message) + "\r\n");
			this.addResendingMessageTask(username, msgID);
			System.out.println("- - - - - Messaggio reinviato col RESEND !!!");
			System.out.println("- - - - - MSG_ID: " + message[0] + "  ");
			System.out.println("- - - - - CMD: " + message[1] + "  ");
			for (int i = 2; i < message.length; i++)
				System.out.println("- - - - - " + message[i] + "  ");
			return true;
		} else if (this.checkIfPlayerIsBot(username)) {
			// Player BOT
			System.out.println("- - Il player e un BOT! Nessun invio "
					+ username);

			return false;
		} else {
			// Player OFFLINE
			System.out
					.println("- - Il player e offline! Aggiungo il messaggio allo stack messaggi!");
			System.out.println("- - Salvo messaggio per" + username);
			for (int j = 0; j < msg.length; j++)
				System.out.println("- - " + msg[j]);
			jpae.messageCreate(new Message(username, msg));

			return false;

		}

	}

	public void receivedACK(Long msgID, Channel channel) {
		messageMap.remove(msgID);
		Player player = playerMap.get(channel);
		if (player != null) {
			playerMap.get(channel).getMessageIdTimeoutMap().get(msgID).cancel();
			playerMap.get(channel).removeMessageWaitedForAck(msgID);
			System.out.println("ACK Ricevuto da "
					+ playerMap.get(channel).getUserData().getUsername()
					+ " per  il msg con ID: " + msgID);
		}

	}

	public void synchronizePlayer(final Channel channel) {

		final String playerName = playerMap.get(channel).getUserData()
				.getUsername();

		List<Game> gameList = jpae.getGameList(playerName);

		if (gameList.size() > 0) {
			System.out.println("Il player " + playerName + " ha "
					+ gameList.size() + " partite  in corso");
			boolean error = false;
			for (int i = 0; i < gameList.size(); i++) {
				Game game = gameList.get(i);
				String[] message = new String[26];

				message[0] = "0801";
				message[1] = String.valueOf(game.getId());
				if (game.getPlayerOne().equals(playerName)) {
					message[2] = game.getPlayerTwo();
					User opponent = jpae.generalUserLookUp(game.getPlayerTwo());
					if (opponent != null) {
						message[3] = String.valueOf(opponent.getLevel());
						message[4] = String.valueOf(opponent.getImage());
						message[5] = String.valueOf(opponent
								.getFacebookPictureURL());
						message[6] = String.valueOf(opponent.getPlayedGames());
						message[7] = String.valueOf(opponent.getWonGames());

						message[8] = String.valueOf(opponent
								.getConsecutiveMatchWon());
						message[9] = String.valueOf(opponent
								.getHighestmatchScore());
						message[10] = String.valueOf(opponent
								.getHighestFirstRoundScore());
						message[11] = String.valueOf(opponent
								.getBestFirstRoundTime());
						message[12] = String.valueOf(opponent
								.getHighestSecondRoundScore());
						message[13] = String.valueOf(opponent
								.getBestSecondRoundTime());
						message[14] = String.valueOf(opponent
								.getHighestThirdRoundScore());
						message[15] = String.valueOf(opponent
								.getBestThirdRoundTime());

						message[16] = String.valueOf(game
								.getPlayerOneScoreOne());
						message[17] = String.valueOf(game
								.getPlayerOneScoreTwo());
						message[18] = String.valueOf(game
								.getPlayerOneScoreThree());
						message[19] = String.valueOf(game
								.getPlayerTwoScoreOne());
						message[20] = String.valueOf(game
								.getPlayerTwoScoreTwo());
						message[21] = String.valueOf(game
								.getPlayerTwoScoreThree());
						message[22] = game.getPlayerWhoGaveUp();
						message[23] = String.valueOf(game
								.isPlayerOneReceivedPills());
						message[24] = game.getChatString();
					} else
						error = true;

				} else {
					message[2] = game.getPlayerOne();
					User opponent = jpae.generalUserLookUp(game.getPlayerOne());
					message[3] = String.valueOf(opponent.getLevel());
					message[4] = String.valueOf(opponent.getImage());
					message[5] = String.valueOf(opponent
							.getFacebookPictureURL());
					message[6] = String.valueOf(opponent.getPlayedGames());
					message[7] = String.valueOf(opponent.getWonGames());

					message[8] = String.valueOf(opponent
							.getConsecutiveMatchWon());
					message[9] = String
							.valueOf(opponent.getHighestmatchScore());
					message[10] = String.valueOf(opponent
							.getHighestFirstRoundScore());
					message[11] = String.valueOf(opponent
							.getBestFirstRoundTime());
					message[12] = String.valueOf(opponent
							.getHighestSecondRoundScore());
					message[13] = String.valueOf(opponent
							.getBestSecondRoundTime());
					message[14] = String.valueOf(opponent
							.getHighestThirdRoundScore());
					message[15] = String.valueOf(opponent
							.getBestThirdRoundTime());

					message[16] = String.valueOf(game.getPlayerTwoScoreOne());
					message[17] = String.valueOf(game.getPlayerTwoScoreTwo());
					message[18] = String.valueOf(game.getPlayerTwoScoreThree());
					message[19] = String.valueOf(game.getPlayerOneScoreOne());
					message[20] = String.valueOf(game.getPlayerOneScoreTwo());
					message[21] = String.valueOf(game.getPlayerOneScoreThree());
					message[22] = game.getPlayerWhoGaveUp();
					message[23] = String.valueOf(game
							.isPlayerTwoReceivedPills());
					message[24] = game.getChatString();
				}

				if (!error)
					this.sendToPlayer(playerName, message);
				error = false;

			}
		}

		scheduler.newTimeout(new TimerTask() {
			@Override
			public void run(Timeout arg0) throws Exception {
				if (isPlayerLoggedIn(playerName)
						&& playerMap.get(channel).getMessageIdTimeoutMap()
								.isEmpty()) {
					playerMap.get(channel).getMessageIdTimeoutMap().clear();
					System.out.println("Invio Fine sincronizzazione!!!");
					String[] endSyncMessage = { "0888" };
					sendToPlayer(playerName, endSyncMessage);
				} else {
					System.out
							.println("Il giocatore  non ha fornito tutti gli ACK previsti, ritento sincronizzazione!!!");
					String[] restartSyncMessage = { "0887" };
					sendToPlayer(playerName, restartSyncMessage);
					synchronizePlayer(channel);
				}
			}
		}, timeForSynchronization, TimeUnit.SECONDS);

	}

	private void deleteGame(String[] msgRcvd, Channel channel) {

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}
		Game game = jpae.gameLookUp(Long.valueOf(msgRcvd[1]));
		String username = playerMap.get(channel).getUserData().getUsername();

		if (game.getPlayerOne().equals(username)) {
			game.setPlayerOneRequestedDelete(true);
			System.out.println("SETTO RICHIESTA PLAYER UNO!!!");
			if (this.checkIfPlayerIsBot(game.getPlayerTwo())) {
				jpae.gameDelete(game);
				game.setPlayerTwoRequestedDelete(true);
			}
		} else {
			game.setPlayerTwoRequestedDelete(true);
			System.out.println("SETTO RICHIESTA PLAYER DUE!!!");
			if (this.checkIfPlayerIsBot(game.getPlayerOne())) {
				jpae.gameDelete(game);
				game.setPlayerOneRequestedDelete(true);
			}
		}

		if ((game.isPlayerOneRequestedDelete() && game
				.isPlayerTwoRequestedDelete())
				|| (game.isPlayerOneRequestedDelete() && jpae
						.generalUserLookUp(game.getPlayerTwo()).isBot())
				|| (game.isPlayerTwoRequestedDelete() && jpae
						.generalUserLookUp(game.getPlayerOne()).isBot())) {
			jpae.gameDelete(game);
		}

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	public void sendChatMessage(String[] msg, Channel channel) {
		String[] msgToSend = { "2001", String.valueOf(msg[1]), msg[2] };

		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		Game game = jpae.gameLookUp(Long.valueOf(msg[1]));
		String playerWhoSentMessage = playerMap.get(channel).getUserData()
				.getUsername();
		String playerWhoReceivedMessage = returnOpponentName(game.getId(),
				playerWhoSentMessage);
		if (game.getPlayerOne().equals(playerWhoSentMessage)) {
			if (game.getChatString().equals("")) {
				System.out.println("Stringa corrente: " + game.getChatString());
				game.setChatString("-_".concat(playerWhoSentMessage)
						.concat("_-").concat((msg[2])));
				System.out.println("Nuova Stringa: " + game.getChatString());
			} else {
				System.out.println("Stringa corrente: " + game.getChatString());
				game.setChatString(game.getChatString().concat("-_666_-")
						.concat("-_").concat(playerWhoSentMessage).concat("_-")
						.concat(msg[2]));
				System.out.println("Nuova Stringa: " + game.getChatString());
			}
		} else {
			if (game.getChatString().equals(""))
				game.setChatString("-_".concat(playerWhoSentMessage)
						.concat("_-").concat((msg[2])));
			else
				game.setChatString(game.getChatString().concat("-_666_-")
						.concat("-_").concat(playerWhoSentMessage).concat("_-")
						.concat(msg[2]));
		}

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

		this.sendToPlayer(playerWhoReceivedMessage, msgToSend);

		System.out.println("Stampo dato salvato: : "
				+ jpae.gameLookUp(Long.valueOf(msg[1])).getChatString());

	}

	private void updateFirstRoundUserRecords(String username, int score,
			float time) {
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		User user = jpae.generalUserLookUp(username);
		if (user.getHighestFirstRoundScore() < score)
			user.setHighestFirstRoundScore(score);
		if (user.getBestFirstRoundTime() < time && time > 0)
			user.setBestFirstRoundTime(time);
		System.out
				.println("UGUALE A NULL??????????????????????????????????????????"
						+ user);
		System.out
				.println("UGUALE A NULL??????????????????????????????????????????"
						+ user.getFirstRoundCounter());
		System.out
				.println("UGUALE A NULL??????????????????????????????????????????"
						+ user.getFirstRoundSum());
		user.setFirstRoundCounter(user.getFirstRoundCounter() + 1);
		user.setFirstRoundSum(user.getFirstRoundSum() + score);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	private void updateSecondRoundUserRecords(String username, int score,
			float time) {
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		User user = jpae.generalUserLookUp(username);
		if (user.getHighestSecondRoundScore() < score)
			user.setHighestSecondRoundScore(score);
		if (user.getBestSecondRoundTime() < time && time > 0)
			user.setBestSecondRoundTime(time);
		user.setSecondRoundCounter(user.getSecondRoundCounter() + 1);
		user.setSecondRoundSum(user.getSecondRoundSum() + score);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	private void updateThirdRoundUserRecords(String username, int score,
			float time) {
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		User user = jpae.generalUserLookUp(username);
		if (user.getHighestThirdRoundScore() < score)
			user.setHighestThirdRoundScore(score);
		if (user.getBestThirdRoundTime() < time && time > 0)
			user.setBestThirdRoundTime(time);
		user.setThirdRoundCounter(user.getThirdRoundCounter() + 1);
		user.setThirdRoundSum(user.getThirdRoundSum() + score);

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	private void updateMatchesUserRecords(String username, int score,
			boolean matchWon) {
		boolean commitRequested = false;
		if (!jpae.getEm().getTransaction().isActive()) {
			jpae.getEm().getTransaction().begin();
			commitRequested = true;
		}

		User user = jpae.generalUserLookUp(username);
		if (user.getHighestmatchScore() < score)
			user.setHighestmatchScore(score);
		if (matchWon) {
			user.setTempConsecutiveMatchesWon(user
					.getTempConsecutiveMatchesWon() + 1);
			if (user.getTempConsecutiveMatchesWon() > user
					.getConsecutiveMatchWon())
				user.setConsecutiveMatchWon(user.getTempConsecutiveMatchesWon());
		} else {
			user.setTempConsecutiveMatchesWon(0);
		}

		if (commitRequested) {
			jpae.getEm().getTransaction().commit();
		}

	}

	private void addNameToBotList() {
		BotNameManager b = new BotNameManager();
		for (String s : b.getNameList())
			jpae.botNameCreate(new BotName(s));

		List<BotName> nameList = jpae.botNameListLookUp();
		for (BotName botName : nameList)
			System.out.println(botName.getUsername() + "     Usato?"
					+ botName.getAlreadyUsed());
	}
}
