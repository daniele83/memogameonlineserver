package com.reborngames.game;

import io.netty.channel.Channel;
import io.netty.util.Timeout;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.reborngames.entity.User;

/**
 * Livello goes from 1 to 999
 * 
 * state: wait draw play
 * 
 * @author emanuele
 */
public class Player {

	public SocketAddress playerSa;
	public Channel playerCh;
	public boolean auth;
	private String state;

	private User userData;
	private Map<Long, Timeout> messageIdTimeoutMap = new HashMap<Long, Timeout>();
	private Integer openGames;

	public Player(User user, Channel c) {
		this.userData = user;
		this.playerCh = c;
	}

	public Player(SocketAddress playerSa) {
		this.playerSa = playerSa;
	}

	public Player(Channel channel) {
		this.playerCh = channel;
	}

	public SocketAddress getPlayerSa() {
		return playerSa;
	}

	public void setPlayerSa(SocketAddress playerSa) {
		this.playerSa = playerSa;
	}

	public boolean isAuth() {
		return auth;
	}

	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public User getUserData() {
		return userData;
	}

	public void setData(User userData) {
		this.userData = userData;
	}

	public Player(Channel channel, long gameId) {
		this.playerCh = channel;
	}

	public Integer getOpenGames() {
		return openGames;
	}

	public void setOpenGames(Integer openGames) {
		this.openGames = openGames;
	}

	public String drawFromDeck() {
		this.state = "play";
		return "0231";
	}

	public String drawFromTrash() {
		this.state = "play";
		return "0241";
	}

	public String dropNew() {
		return "0251";
	}

	public String dropAdd() {
		return "0261";
	}

	public String discardCard() {
		// TODO
		this.state = "wait";
		return "0271";
	}

	public Channel getChannel() {
		return playerCh;
	}

	private void setPlayerCh(Channel playerCh) { // modified to private
		this.playerCh = playerCh;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void addMessageWaitingForAck(Long l, Timeout t) {
		this.messageIdTimeoutMap.put(l, t);
	}

	public void removeMessageWaitedForAck(Long l) {
		this.messageIdTimeoutMap.remove(l);
	}

	public Map<Long, Timeout> getMessageIdTimeoutMap() {
		return messageIdTimeoutMap;
	}
	

}
