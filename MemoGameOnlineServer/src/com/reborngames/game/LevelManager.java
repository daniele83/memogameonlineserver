package com.reborngames.game;

import com.reborngames.entity.User;

public class LevelManager {

	private int levelMin;
	private int[] levelMax;

	public LevelManager() {

		buildLevels();
	}

	private void buildLevels() {

		levelMin = 0;
		levelMax = new int[1000];
		for (int i = 1; i < 1000; i++) {
			levelMax[i] = 9000 * (i);
		}
	}

	public int checkLevelIncreasing(User user, int exp) {
		if (user.getExp() + exp > (levelMax[user.getLevel()] - levelMin)) {
			System.out.println("EXP attuali: " + user.getExp());
			System.out.println("EXP guadagnati col Round: " + exp);
			System.out.println("EXP TOTALI: " + (user.getExp() + exp));
			System.out.println("EXP RANGE dell'attuale livello: "
					+ levelMax[user.getLevel()] + " (livello "
					+ user.getLevel() + ")");
			return (user.getExp() + exp - (levelMax[user.getLevel()] - levelMin));
		}
		System.out.println("EXP attuali: " + user.getExp());
		System.out.println("EXP guadagnati col Round: " + exp);
		System.out.println("EXP TOTALI: " + (user.getExp() + exp));
		System.out.println("EXP RANGE dell'attuale livello: "
				+ levelMax[user.getLevel()] + " (livello "
				+ user.getLevel() + ")");
		return 0;

	}

	public int getExpOfCurrentLevel(User user) {

		return (levelMax[user.getLevel()] - levelMin);
	}

}
