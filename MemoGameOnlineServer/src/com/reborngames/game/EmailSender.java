package com.reborngames.game;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSender {

	public EmailSender() {
	}

	public static void sendEmail(String toAddress,
			String memoGameAccountUsername, String memoGameAccountPassword) {

		final String username = "memogameonline@gmail.com";
		final String password = "GiocoDiMemoria2016";

		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("memogameonline@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toAddress));
			message.setSubject("Email Di Prova");
			message.setText("Dear "
					+ memoGameAccountUsername
					+ ",\nThis is an automatic eMail sent to you to recover the password of your account.\n\n"
					+ " Username: " + memoGameAccountUsername + "\n Password: "
					+ memoGameAccountPassword+"\n\nBest,\nThe Memo Game Online team");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
