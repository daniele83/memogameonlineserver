package com.reborngames.game;

import java.util.ArrayList;

public class BotNameManager {

	private ArrayList<String> nameList;

	public BotNameManager() {
		nameList = new ArrayList<String>();
		nameList.add("Nannino50");
		nameList.add("MeinKopf");
		nameList.add("Lugano1981");
		nameList.add("HerrMiele");
		nameList.add("JovanottiZ");
		nameList.add("FreeAsBird");
		nameList.add("desdemona");
		nameList.add("CiroForzaNapoli");
		nameList.add("naiv666");
		nameList.add("fracido90");
		nameList.add("Heinz");
		nameList.add("MonamourFuckYou");
		nameList.add("LambertoDini");
		nameList.add("Hurricane666");
		nameList.add("lunetta");
		nameList.add("123Stella");
		nameList.add("Funzine");
		nameList.add("nothingBUT_99");
		nameList.add("GianniMorandi");
		nameList.add("cagna85");
		nameList.add("IammIamm");
		nameList.add("mark444");
		nameList.add("BastianichContrario");
		nameList.add("Honoris_ABBA");
		nameList.add("NibelungRing");
		nameList.add("chairotto");
		nameList.add("MoniMoni");
		nameList.add("Monacoooo");
		nameList.add("antoninoSCIASCIA");
		nameList.add("scimmio");
		nameList.add("Nokyo246");
		nameList.add("iettato83");
		nameList.add("gargoyle");
		nameList.add("marcoCarta85");
		nameList.add("fuckYou");
		nameList.add("dromedarioGobbo");
		nameList.add("cagna92");
		nameList.add("Luca_Fiesole");
		nameList.add("montevideo");
		nameList.add("ConsoleRomano");
		nameList.add("IellaIellaccia");
		nameList.add("HoldON55");
		nameList.add("Bidello68");
		nameList.add("narcosBrazil5");
		nameList.add("mortacciTua1901");
		nameList.add("giumangi");
		nameList.add("MarcoAv");
		nameList.add("freedom85");
		nameList.add("DockStock");
		nameList.add("lambretta666");
		nameList.add("Munich1860");
		nameList.add("ombroso");
		nameList.add("6morto");
		nameList.add("nikitaCMajor");
		nameList.add("morituriTeCia");
		nameList.add("GanzeGanzen");
		nameList.add("HammeronOn");
		nameList.add("ancona56");
		nameList.add("frivola87");
		nameList.add("manicomio");
		nameList.add("barilesecco");
		nameList.add("cincilla");
		nameList.add("terremoto89");
		nameList.add("nosebleed");
		nameList.add("Dresden44");
		nameList.add("MastellaUnoDiNoi");
		nameList.add("Gregor78");
		nameList.add("MaxPezzali");
		nameList.add("HaHaHaHa");
		nameList.add("druckenjetz");
		nameList.add("Pregianza85");
		nameList.add("MonkeyBrazz");
		nameList.add("MeinStuhle");
		nameList.add("Fracchia81");
		nameList.add("asdasd");
		nameList.add("gaga");
		nameList.add("nicaragua89");
		nameList.add("madooooo");
		nameList.add("drink_eat");
		nameList.add("camorrista");
		nameList.add("barilelorenzo");
		nameList.add("scoppiato83");
		nameList.add("morigerato69");
		nameList.add("camosciodoro");
		nameList.add("apostroforosa");
		nameList.add("giogio");
		nameList.add("formaggina95");
		nameList.add("camorrista85");
		nameList.add("FlorenceMachine");
		nameList.add("MoreMorphine");
		nameList.add("dadaism");
		nameList.add("pigmalione");
		nameList.add("Cadeddu");
		nameList.add("Frappe");
		nameList.add("skalkagnato");
		nameList.add("Nichelle");
		nameList.add("GannisGreek");
		nameList.add("VenneriBecci");
		nameList.add("Midiot!");
		nameList.add("Bofonchiare");
		nameList.add("Miriam");
		nameList.add("dumb4YOU");
		nameList.add("Franzoni86");
		nameList.add("SoloBelvedere");
		nameList.add("BastaToni");
		nameList.add("ManyMeaning");
		nameList.add("NoMaybeYes_00");
		nameList.add("CamorraStato");
		nameList.add("grooveBitch");
		nameList.add("Arcibald_KO");
		nameList.add("ArmandoRossi");
		nameList.add("provoloneAsD");
		nameList.add("bettySMACK");
		nameList.add("DoNotDoThat");
		nameList.add("maradona90");
		nameList.add("Notwist83");
		nameList.add("username");
		nameList.add("FraMartino");
		nameList.add("AdinoliGrassone");
		nameList.add("annarumma");
		nameList.add("grimaldello");
		nameList.add("DonGiovanni");
		nameList.add("NaschimyKasaky");
		nameList.add("PierPaoloFagiolini");
		nameList.add("Isaac");
		nameList.add("BigMouth");
		nameList.add("DomAndJerry");
		nameList.add("fiesole99");
		nameList.add("brodolino");
		nameList.add("GrantJohn");
		nameList.add("cherubina94");
		nameList.add("Nonrompere");
		nameList.add("mercury_345");
		nameList.add("rubentino2006");
		nameList.add("Afragolaa");
		nameList.add("Prrrr");
		nameList.add("Nazinazi");
		nameList.add("34_cianciare");
		nameList.add("DanceAndDrink");
		nameList.add("Frappuccino");
		nameList.add("GiangiGiangi");
		nameList.add("MapoMapo1");
	}

	public ArrayList<String> getNameList() {
		return nameList;
	}

	public void setNameList(ArrayList<String> nameList) {
		this.nameList = nameList;
	}
	

}
