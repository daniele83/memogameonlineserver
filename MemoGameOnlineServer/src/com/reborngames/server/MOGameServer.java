package com.reborngames.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class MOGameServer {

	private int port;

	public MOGameServer(int port) {
		this.port = port;
	}

	public static void main(String[] args) throws Exception {
		int port;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		} else {
			port = 64000;
		}
		System.out.println("Server Running...sulla PORTA: " + port);
		new MOGameServer(port).run();

	}

	public void run() throws Exception {
		// final List<Game> games = new ArrayList<Game>();
		// final BurracoGameServerAssistant bgsa = new
		// BurracoGameServerAssistant();

		// Si occupera di creare i canali
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		// Si occupera di servire i canali
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			// Associo i workgroups al server
			b.group(bossGroup, workerGroup)
			// Istanzio un nuovo canale di tipo NioServerSocketChannel.class
					.channel(NioServerSocketChannel.class)

					// .handler(new LoggingHandler(LogLevel.INFO))

					// Associo l'handler ServerHandler al canale appena creato
					.childHandler(new MOGameServerAssistant())
					// La documentazione non ha una descrizione per questa
					// opzione
					.option(ChannelOption.SO_BACKLOG, 128)
					// La documentazione non ha una descrizione per questa
					// opzione
					.childOption(ChannelOption.SO_KEEPALIVE, true);

			// ****************************************************************

			// ****************************************************************

			// Sincronizziamo su porta port un canale f in attesa di risposta
			ChannelFuture f = b.bind(port).sync();
			// Chiude il canale f dopo l'arrivo della risposta
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

}
