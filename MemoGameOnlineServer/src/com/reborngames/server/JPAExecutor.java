package com.reborngames.server;

import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.RollbackException;

import com.reborngames.entity.BotName;
import com.reborngames.entity.Game;
import com.reborngames.entity.Message;
import com.reborngames.entity.User;

public class JPAExecutor {

	private EntityManagerFactory emf;
	private EntityManager em;

	// FIXME ASSICURARSI DI GESTIRE LE ECCEZIONI PER IL AUTORECONNECT TRUE (la
	// query che avvia il reconnect fallisce)

	public JPAExecutor(String persistenceUnitName) {
		emf = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = emf.createEntityManager();
	}

	/**
	 * SIGNUP
	 * 
	 * @param user
	 * @return
	 */
	public String userCreate(User user) {
		String result = "TRUE";

		boolean commitRequested = false;
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				commitRequested = true;
			}
			em.persist(user);

			if (commitRequested) {
				em.getTransaction().commit();

			}
		} catch (NoResultException e) {
			result = "ERROR_CREATING_USER";
		}

		return result;
	}

	public String gameCreate(Game game) {
		String result = "TRUE";

		// Verifico unicita game (due aprtite tra gli stessi player non sono
		// amissibili)
		// this.jpaCheckGameUniqueness(playerOne, playerTwo);
		boolean commitRequested = false;
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				commitRequested = true;
			}
			em.persist(game);
			if (commitRequested) {
				em.getTransaction().commit();
			}
		} catch (RollbackException eTwo) {
			result = "ERROR_CREATING_GAME";
		}

		return result;
	}

	public String messageCreate(Message obj) {
		String result = "TRUE";

		// Verifico unicita game (due aprtite tra gli stessi player non sono
		// amissibili)
		// this.jpaCheckGameUniqueness(playerOne, playerTwo);
		boolean commitRequested = false;
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				commitRequested = true;
			}
			em.persist(obj);
			if (commitRequested) {
				em.getTransaction().commit();
			}

		} catch (RollbackException eTwo) {
			result = "ERROR_CREATING_MESSAGE";
		}

		return result;
	}

	public String botNameCreate(BotName botName) {
		String result = "TRUE";

		BotName temp;
		Query query = em
				.createQuery("select botname from BotName botname WHERE botname.username = :value1");
		query.setParameter("value1", botName.getUsername());

		try {
			temp = (BotName) query.getSingleResult();
		} catch (NoResultException e) {
			System.out
					.println("Risultato non tovato! POSSO SALVARE  ILO NOME!!!");
			boolean commitRequested = false;
			try {
				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
					commitRequested = true;

				}
				em.persist(botName);

				if (commitRequested) {
					em.getTransaction().commit();
				}

			} catch (RollbackException eTwo) {
				result = "ERROR_CREATING_BOTNAME";
			}
		}

		return result;
	}

	public User generalUserLookUp(String username, String password) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE u.username = :value1 AND u.password = :value2");
		query.setParameter("value1", username);
		query.setParameter("value2", password);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public List<BotName> botNameListLookUp() {
		List<BotName> nameList;

		Query query = em.createQuery("select u from BotName u");
		// query.setParameter("value2", true);
		try {
			nameList = (List<BotName>) query.getResultList();
		} catch (NoResultException e) {
			nameList = null;
		}

		return nameList;
	}

	public String botNameGenerate() {
		Query query = em
				.createQuery("select u from BotName u WHERE  u.alreadyUsed is false");

		try {

			List<BotName> nameList = (List<BotName>) query.getResultList();
			BotName botName = nameList.get(0);

			// SALVO IL BOT COME USATO!!!!!!
			boolean commitRequested = false;
			try {
				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
					commitRequested = true;
				}
				botName.setAlreadyUsed(true);
				em.persist(botName);
				if (commitRequested) {
					em.getTransaction().commit();
				}

			} catch (RollbackException eTwo) {
				System.out.println("PROBLEMI!!!!");
			}

			return botName.getUsername();
		} catch (NoResultException e) {
			return null;
		}

	}

	public User userLookUpFacebookId(String facebookId) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE  u.facebookId = :facebookId");
		query.setParameter("facebookId", facebookId);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public User generalUserLookUp(String username) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE u.username = :value");

		query.setParameter("value", username);
		// query.setParameter("value2", false);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public User userEmailBasedLookUp(String eMail) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE u.email = :email");

		query.setParameter("email", eMail);
		// query.setParameter("value2", false);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public User humanUserLookUp(String username) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE u.username = :value1 AND u.bot is false");

		query.setParameter("value1", username);
		// query.setParameter("value2", false);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public User botLookUp(String username) {
		User user;

		Query query = em
				.createQuery("select u from User u WHERE u.username = :value1 AND u.bot is true");
		query.setParameter("value1", username);
		// query.setParameter("value2", true);
		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}

		return user;
	}

	public List<User> botsLevelBasedLookUp(int levelMin, int levelMax) {
		List<User> userList;

		Query query = em
				.createQuery("select u from User u WHERE u.level >= :levelMin AND u.level <= :levelMax "
						+ "AND u.bot is true");
		query.setParameter("levelMin", levelMin);
		query.setParameter("levelMax", levelMax);
		// query.setParameter("value2", true);
		try {
			userList = (List<User>) query.getResultList();
		} catch (NoResultException e) {
			userList = null;
		}

		return userList;
	}

	public Game gameLookUp(Long gameId) {
		Game game;

		Query query = em
				.createQuery("select g from Game g WHERE g.id = :value1");
		query.setParameter("value1", gameId);
		try {
			game = (Game) query.getSingleResult();
		} catch (NoResultException e) {
			game = null;
		}

		return game;
	}

	public List<Game> getGameList(String playerName) {
		List<Game> gameList;

		Query query = em
				.createQuery("select g from Game g WHERE (g.playerOne= :playername AND g.playerOneRequestedDelete is false)"
						+ " OR (g.playerTwo= :playername AND g.playerTwoRequestedDelete is false)");
		query.setParameter("playername", playerName);
		try {
			gameList = (List<Game>) query.getResultList();
		} catch (NoResultException e) {
			gameList = null;
			System.out.println("NON TROVO NULLA!!!!");
		}

		return gameList;
	}

	public List<Message> getMessageList(String playerName) {
		List<Message> message;

		Query query = em
				.createQuery("select m from Message m WHERE m.playerName = :value");
		query.setParameter("value", playerName);
		try {
			message = (List<Message>) query.getResultList();
		} catch (NoResultException e) {
			message = null;
			System.out.println("NON TROVO NULLA!!!!");
		}

		return message;
	}

	public boolean gameAlreadyExists(String playerOne, String playerTwo) {

		Query firstQuery = em
				.createQuery("select g from Game g WHERE ((g.playerOne = :value1 AND g.playerTwo = :value2) "
						+ "OR (g.playerOne = :value2 AND g.playerTwo = :value1)) AND g.closed is false");
		firstQuery.setParameter("value1", playerOne);
		firstQuery.setParameter("value2", playerTwo);
		try {
			firstQuery.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		}

	}

	public Query jpaRead(String query) {
		Query q = em.createQuery(query);
		return q;
	}

	public void jpaUpdate(Object obj) {
		boolean commitRequested = false;

		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			commitRequested = true;
		}

		em.merge(obj);
		em.getTransaction().commit();
		em.detach(obj);

		if (commitRequested) {
			em.getTransaction().commit();
		}
	}

	public void jpaDelete(Object obj) {

		boolean commitRequested = false;

		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			commitRequested = true;
		}
		em.remove(obj);
		if (commitRequested) {
			em.getTransaction().commit();
		}

	}

	public void messageDelete(Message msg) {

		boolean commitRequested = false;

		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			commitRequested = true;

		}
		em.remove(msg);

		if (commitRequested) {
			em.getTransaction().commit();
		}

	}

	public void gameDelete(Game game) {

		boolean commitRequested = false;

		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			commitRequested = true;

		}

		em.remove(game);

		if (commitRequested) {
			em.getTransaction().commit();
		}

	}

	public void closeLogicaJPA() {
		em.close();
		emf.close();
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public void resetDB() {
		boolean commitRequested = false;

		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			commitRequested = true;

		}

		List<Message> messageList = em.createQuery("select m from Message m ")
				.getResultList();
		List<User> userList = em.createQuery("select u from User u ")
				.getResultList();
		List<Game> gameList = em.createQuery("select g from Game g ")
				.getResultList();
		for (Message m : messageList)
			em.remove(m);
		for (User u : userList)
			em.remove(u);
		for (Game g : gameList)
			em.remove(g);

		if (commitRequested) {
			em.getTransaction().commit();
		}
	}

}
