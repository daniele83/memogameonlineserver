package com.reborngames.server;

import java.io.StringReader;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.ReadTimeoutHandler;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.reborngames.entity.Message;

/**
 * CONDIZIONE 1: tempo di risposta del server ad un client non superiore a 15s
 * se questo periodo di tempo viene superato, il client dara
 * UNABLE_TO_CONNECT_TO_SERVER
 * 
 * @author Emanuele
 */
public class MOGameServerHandler extends ChannelDuplexHandler {

	private MOGameServerAssistant mogsa;
	private Gson gson;
	private String[] output;

	public MOGameServerHandler(MOGameServerAssistant moGameServerAssistant) {
		this.mogsa = moGameServerAssistant;
		gson = new Gson();
		output = null;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		mogsa.hostJoined(ctx.channel());
		ctx.writeAndFlush(gson.toJson("0100") + "\r\n");
		System.out.println("CLIENT CONNESSO al ChannelHandlerContext: " + ctx);
		System.out.println("CLIENT CONNESSO al canale: " + ctx.channel());
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("1) Invoco:  channelInactive(channel)");
		mogsa.hostLeft(ctx.channel());
		// System.out.println("CLIENT DISCONNESSO: " + ctx.channel());
		// ctx.deregister();
		// ctx.disconnect();
	}

	/**
	 * ***********************************************************************
	 * Ricordare l'escape string lato client /n
	 * ***********************************************************************
	 */
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		// Stampo solo se non � il messaggio di sincronizzazione
		System.out.println(((String) msg).trim());
		JsonReader jr = new JsonReader(new StringReader(((String) msg).trim())); 
		jr.setLenient(true); 
		String[] MsgRcv = gson.fromJson(((String) msg).trim(), String[].class);
		//String[] MsgRcv = gson.fromJson(jr, String[].class);
		if (!MsgRcv[0].equals("9990")) {
			System.out.println("___");
			System.out.println("Messaggio ricevuto di lunghezza "
					+ MsgRcv.length);
			System.out.println("CMD: " + MsgRcv[0]);
			for (int i = 1; i < MsgRcv.length; i++) {
				System.out.println("Elemento " + i + " : " + MsgRcv[i]);
			}
			// System.out.println("Canale di connessione " + ctx.channel());
			System.out.println("___\n");
		}

		mogsa.lobby.servePlayer(MsgRcv[0], MsgRcv, ctx.channel());

	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// System.out.println("Message received from: " + ctx.channel());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		System.out.println("Sparata eccezione!!!");
		cause.printStackTrace();
		System.out.println("ERROR CAUSE: " + cause.getClass().getName());
		// ctx.close();

	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
			throws Exception {
		System.out.println("Sparato evento utente!!!!");
		if (evt instanceof IdleStateEvent) {
			IdleStateEvent e = (IdleStateEvent) evt;
			if (e.state() == IdleState.READER_IDLE) {
				// ctx.close();
				System.out
						.println("Nessuna lettura negli ultimi 30 secondi !!! forzo il LogOout del Player e la chiusura del canale!!"
								+ "");
				//TODO
				mogsa.hostLeft(ctx.channel());
				ctx.channel().close();

			} else if (e.state() == IdleState.WRITER_IDLE) {
				// ctx.writeAndFlush(new PingMessage());
				System.out
						.println("Nessuna scrittura negli ultimi 5 secondi !!!");
			} else if (e.state() == IdleState.ALL_IDLE) {
				// ctx.writeAndFlush(new PingMessage());
				System.out
						.println("Nessuna scrittura/lettura negli ultimi 5 secondi !!!");
			}
		}
		//ctx.pipeline().fireUserEventTriggered("");
	}
}
