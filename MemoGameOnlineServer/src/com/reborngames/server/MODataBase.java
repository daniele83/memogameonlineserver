package com.reborngames.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MODataBase {

	private String userName;
	private String password;
	private String dbms;
	private String serverName;
	private String dbPortNumber;
	private String dbName;

	public MODataBase() {
		userName = "root";
		password = "root";
		dbms = "mysql";
		serverName = "localhost";
		dbPortNumber = "3306";
		dbName = "test";
	}

	public Connection getConnection() throws SQLException {
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.userName);
		connectionProps.put("password", this.password);

		conn = DriverManager.getConnection("jdbc:" + this.dbms + "://"
				+ this.serverName + ":" + this.dbPortNumber + "/",
				connectionProps);

		System.out.println("Connected to database");
		return conn;
	}
}
