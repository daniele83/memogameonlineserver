package com.reborngames.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.reborngames.game.Lobby;

import static java.util.concurrent.TimeUnit.*;

public class MOGameServerAssistant extends ChannelInitializer<SocketChannel> {

	public Lobby lobby;
	public MODataBase modb;
	public Connection conn;
	public JPAExecutor jpae;
	private final ScheduledExecutorService executor = Executors
			.newScheduledThreadPool(4);
	private final static int readTimeout = 30;

	public MOGameServerAssistant() {
		jpae = new JPAExecutor("mosmysql");
		// botF = new BotFactory();
		lobby = new Lobby(jpae);

		this.poolDB();
		// TODO istanzio bots al riavvio

		/*
		 * modb = new MODataBase(); try { conn = modb.getConnection();
		 * System.out.println("DB: " + conn.isValid(10)); } catch (SQLException
		 * e) { e.printStackTrace(); } try { conn.close(); } catch (SQLException
		 * e) { e.printStackTrace(); }
		 */
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		// pipeline.addLast("readTimeoutHandler", new ReadTimeoutHandler(
		// readTimeoutHandlerTime));
		pipeline.addLast("idleStateHandler", new IdleStateHandler(readTimeout,
				0, 0));
		pipeline.addLast("frameDecoder", new LineBasedFrameDecoder(300));
		pipeline.addLast("stringDecoder", new StringDecoder(CharsetUtil.UTF_8));
		pipeline.addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
		pipeline.addLast("handler", new MOGameServerHandler(this));

		System.out.println(pipeline);

		// pipeline.removeFirst();
		// System.out.println(pipeline);

	}

	public void hostJoined(Channel channel) {

	}

	public void hostLeft(Channel channel) {
		System.out.println("2) Invoco:  hostLeft(channel)");

		if (lobby.isPlayerLoggedIn(channel)) {
			System.out.println("_log Out: "
					+ lobby.playerMap.get(channel).getUserData().getUsername());
			lobby.playerList.remove(lobby.playerMap.get(channel));
			lobby.playerMap.remove(channel);
		}
	}

	// TODO deve fare pooling su versione client
	public void poolDB() {
		ScheduledFuture<?> future = executor.scheduleAtFixedRate(
				new Runnable() {
					@Override
					public void run() {
						System.out.println("Pooling Eseguito!");
						if (jpae.generalUserLookUp("daniele")!=null && jpae.generalUserLookUp("daniele").getUsername()
								.equals("daniele"))
							System.out.println("Pooling Eseguito! ''daniele'' tovato!!!");
						else
							System.out.println("Pooling Eseguito! ''daniele'' non tovato!!!");
					}
				}, 1, 1, TimeUnit.HOURS);

	}
}